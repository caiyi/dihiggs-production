import matplotlib.pyplot as plt
import numpy as np
import math
import sys
from scipy import interpolate
import scan.cxSM as cxSM
from higgs_decay_width import *
import fileinput
import os

input_file=['/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/output/40/result.txt','/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/output/41/result.txt']
#input_file=['output/1/result.txt']
for n in range(2500):
    input_file.append(os.path.join('first_2',str(n),'result.txt'))
for n in range(2500):
    input_file.append(os.path.join('first_3',str(n),'result.txt'))

data=np.loadtxt(fileinput.input(input_file))
x2=[300,400,500]
y2=[0.010532,0.00510299,0.00229842]
x3=[300,700,1000]
y3=[0.002354,0.00100718,0.000685853]

omega=data[:,14]
strength=data[:,7]
mp=938.2720813*0.001
v0=246.0*np.ones(len(data))
a1=data[:,0]
x0=data[:,1]
s1=data[:,2]
c1=np.sqrt(1-s1**2)
mh=data[:,3]
ms=data[:,4]
mA=data[:,5]
order=data[:,6]
mh2=mh**2
ms2=ms**2
mA2=mA**2
delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
y=data[:,8]*10**(-36)
y=y*omega/0.1196

ary_a1=np.array(a1); ary_x0=np.array(x0); ary_v0=np.array(v0); ary_s1=np.array(s1)
ary_mh2=np.array(mh2); ary_ms2=np.array(ms2); ary_mA2=np.array(mA2)

cxSM_decay_class = cxSM_decay_width(ary_a1, ary_x0, ary_v0, ary_s1, ary_mh2, ary_ms2, ary_mA2)
cro_sec_h2_1 = cxSM_decay_class.cro_sec_2body_tmp()
cro_sec_h2_2 = cxSM_decay_class.cro_sec_2body()
cro_sec_h2_3 = cxSM_decay_class.cro_sec_3body()
cro_sec_h2_h1h1 = cxSM_decay_class.xsec_h2_h1h1()
br_h_AA = cxSM_decay_class.BR_h1_AA()
bbbb = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbbb.txt', header=None, delimiter="\t")
bbbb_func = interpolate.interp1d(bbbb.iloc[:,0].values,bbbb.iloc[:,1].values,kind='linear')
bbtt = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbtt.txt', header=None, delimiter="\t")
bbtt_func = interpolate.interp1d(bbtt.iloc[:,0].values,bbtt.iloc[:,1].values,kind='linear')
bbyy = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbyy.txt', header=None, delimiter="\t")
bbyy_func = interpolate.interp1d(bbyy.iloc[:,0].values,bbyy.iloc[:,1].values,kind='linear')

idx4 = np.where((cro_sec_h2_2>0.0) & (s1>-0.274) & (s1<0.274) & (y<5.99e-47) & (omega<0.1196) & (cro_sec_h2_h1h1 < bbtt_func(ms)) & (cro_sec_h2_h1h1 < bbbb_func(ms)) & (cro_sec_h2_h1h1 < bbyy_func(ms)))
idx5 = np.where((cro_sec_h2_3>0.0) & (s1>-0.274) & (s1<0.274) & (y<1.30e-46) & (omega<0.1196) & (cro_sec_h2_h1h1 < bbtt_func(ms)) & (cro_sec_h2_h1h1 < bbbb_func(ms)) & (cro_sec_h2_h1h1 < bbyy_func(ms)))
'''
fig1,ax1=plt.subplots(figsize=(8,8), dpi=80)
Cro_Sec_1 = ax1.scatter(ms[idx2],0.019*cro_sec_h2_1[idx2],s=0.6,c=strength[idx2])
ax1.set_yscale('log')
ax1.set_xlabel(r'$M_{h_2} (GeV)$')
ax1.set_ylabel(r'$\sigma (pb)$')
fig1.colorbar(Cro_Sec_1)
fig1.savefig("cross_2body_BR0.1.png")

fig2,ax2=plt.subplots(figsize=(8,8), dpi=80)
Cro_Sec_2 = ax2.scatter(ms[idx2],cro_sec_h2_2[idx2],s=0.6,c=strength[idx2])
ax2.set_yscale('log')
ax2.set_xlabel(r'$M_{h_2} (GeV)$')
ax2.set_ylabel(r'$\sigma (pb)$')
fig2.colorbar(Cro_Sec_2)
fig2.savefig("cross_2body.png")

fig3,ax3=plt.subplots(figsize=(8,8), dpi=80)
Cro_Sec_3 = ax3.scatter(ms[idx3],cro_sec_h2_3[idx3],s=0.6,c=strength[idx3])
ax3.set_yscale('log')
ax3.set_xlabel(r'$M_{h_2} (GeV)$')
ax3.set_ylabel(r'$\sigma (pb)$')
fig3.colorbar(Cro_Sec_3)
fig3.savefig("cross_3body.png")
'''
fig=plt.figure(figsize=(15,6), dpi=150)
fig1 = fig.add_subplot(121)
fig2 = fig.add_subplot(122)

Cro_Sec_4 = fig1.scatter(ms[idx4],0.019*cro_sec_h2_1[idx4],s=0.6,c=strength[idx4],cmap='Blues',alpha=0.8)
Cro_Sec_5 = fig2.scatter(ms[idx5],cro_sec_h2_3[idx5],s=0.6,c=strength[idx5],cmap='Oranges',alpha=0.8)
fig1.set_yscale('log')
fig1.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig1.set_ylabel(r'$\sigma_{2~body}~\mathrm{[pb]}$',size=20)
fig2.set_yscale('log')
fig2.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig2.set_ylabel(r'$\sigma_{3~body}~\mathrm{[pb]}$',size=20)
cb1=fig.colorbar(Cro_Sec_4,ax=fig1)
cb2=fig.colorbar(Cro_Sec_5,ax=fig2)
l1,=fig1.plot([300,400,500],[0.0107995,0.00497448,0.0021281],color='black',linewidth=2,label='95% CLs')
fig1.plot([500,700,1000],[0.00217955,0.000796386,0.000685853],color='black',linewidth=2)
l2,=fig1.plot([300,400,500],[0.0107995/1.96*5,0.00497448/1.96*5,0.0021281/1.96*5],color='gray',linewidth=2,label='5 Sigma')
fig1.plot([500,700,1000],[0.00217955/1.96*5,0.000796386/1.96*5,0.000685853/1.96*5],color='gray',linewidth=2)
fig1.legend(handles=[l2,l1],fontsize=20,loc=3)
l1,=fig2.plot([300,400,500],[0.0107995,0.00497448,0.0021281],color='black',linewidth=2,label='95% CLs')
fig2.plot([500,700,1000],[0.00217955,0.000796386,0.000685853],color='black',linewidth=2)
l2,=fig2.plot([300,400,500],[0.0107995/1.96*5,0.00497448/1.96*5,0.0021281/1.96*5],color='gray',linewidth=2,label='5 Sigma')
fig2.plot([500,700,1000],[0.00217955/1.96*5,0.000796386/1.96*5,0.000685853/1.96*5],color='gray',linewidth=2)
fig2.legend(handles=[l2,l1],fontsize=20,loc=3)
fig1.set_xlim([300,1000])
fig2.set_xlim([300,1000])
fig.savefig("cross_body.png")