import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import sys 
from higgs_decay_width import *
import os
import fileinput
input_file=['/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/first_order_2body.dat',
           '/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/first_order_3body.dat'
          ]
data=np.loadtxt(fileinput.input(input_file))

color=data[:,8]/np.sqrt(2)
v=246.0*np.ones(len(data))
a1=data[:,0]
a13=-np.power(-a1,1./3)
vs=data[:,1]
sin=data[:,2]
theta=np.arcsin(sin)
cos=np.sqrt(1-sin**2)
mh=data[:,3]
ms=data[:,4]
mA=data[:,5]
mh2=mh**2
ms2=ms**2
mA2=mA**2
tmp=-np.power(-a1/vs**3,1./3)
delta2=(2*(mh2-ms2)*cos*sin)/(vs*v)

fig=plt.figure(figsize=(10,5), dpi=300)
fig1 = fig.add_subplot(121)
fig2 = fig.add_subplot(122)

Theta=fig1.scatter(ms[:],theta[:],s=0.6,c=color[:])
A1=fig2.scatter(ms[:],a13[:],s=0.6,c=color[:])

fig1.set_xlabel(r'$M_S (GeV)$')
fig1.set_ylabel(r'$\theta$')
fig2.set_xlabel(r'$M_S (GeV)$')
fig2.set_ylabel(r'$a_1^{1/3}$')

fig.colorbar(Theta, ax=fig1)
fig.colorbar(A1, ax=fig2)
fig.savefig("p2.png")