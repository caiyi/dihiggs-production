import matplotlib.pyplot as plt
import numpy as np
from higgs_decay_width import *
import os
import fileinput
theta = ["-22.0","-20.5","-15.0"]
df_1 = np.loadtxt("XENON1T.csv")
df_n = np.loadtxt("XENONnT.csv")
x_1=df_1[:,0]
y_1=df_1[:,1]
x_n=df_n[:,0]
y_n=df_n[:,1]

fig1,ax1=plt.subplots(figsize=(8,6), dpi=300)

fig2,ax2=plt.subplots(figsize=(8,6), dpi=300)

for i in (0,1,2):
    print(i)
    data=np.loadtxt("/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/chengwei/{}/result.txt".format(str(i)))

    color=[]
    omega=data[:,13]
    mp=938.2720813*0.001
    v=246.0*np.ones(len(data))
    a1=data[:,0]
    vs=data[:,1]
    sin=data[:,2]
    cos=np.sqrt(1-sin**2)
    mh=data[:,3]
    ms=data[:,4]
    mA=data[:,5]
    order=data[:,6]
    mh2=mh**2
    ms2=ms**2
    mA2=mA**2
    delta2=(2*(mh2-ms2)*cos*sin)/(vs*v)
    '''
    cxSM_decay_class = cxSM_decay_width(a1,vs,v,sin,mh2,ms2,mA2)
    g_111, g_211, g_1AA, g_2AA = cxSM_decay_class.cxSM_coupling()
    '''
    g_1AA=(np.sqrt(2)*a1+mh2*vs)*sin/(2*vs**2) 
    g_2AA=(np.sqrt(2)*a1+ms2*vs)*cos/(2*vs**2)

    fu=0.020
    fd=0.026
    fs=0.118
    fT=1-fu-fd-fs
    y=3.9*10**(-28)*mp**4/(2*np.pi*v**2*(mp+mA)**2)*(g_1AA*cos/mh2-g_2AA*sin/ms2)**2*(fu+fd+fs+fT*2/9)**2
    """
    index1 = np.where(omega>0.11)
    ax2.scatter(mA[index1],y[index1],label=theta[i],s=0.5)
    y=y*omega/0.1196
    index2 = np.where(omega<=0.11)
    ax2.scatter(mA[index2],y[index2],label=theta[i],s=0.5)
    ax1.scatter(mA,omega,label=theta[i],s=0.5)
    """
    z=data[:,8]*10**(-36)
    y=z*omega/0.1196
    ax2.scatter(mA,y,label=theta[i],s=0.5)
    ax1.scatter(mA,omega,label=theta[i],s=0.5)

ax1.set_xlabel('$\mathrm{M_A (GeV)}$')
ax1.set_ylabel('$\mathrm{\Omega_A h^2}$')
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylim(10e-8, 10e-1)
ax1.set_xlim(50, 10000)
ax1.legend()
fig1.savefig("sigma_omega_chengwei.png")
ax2.set_xlabel('$\mathrm{M_A (GeV)}$')
ax2.set_ylabel('$\mathrm{rescaled \sigma_{SI} (cm^2)}$')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.set_ylim(10e-49, 10e-43)
ax2.set_xlim(50, 10000)
ax2.plot(x_1,y_1,color='black',linewidth=2,label='XENON1T')
ax2.plot(x_n,y_n,color='black',linewidth=2,linestyle='--',label='XENONnT')
ax2.legend()
fig2.savefig("sigma_sigma_chengwei.png")