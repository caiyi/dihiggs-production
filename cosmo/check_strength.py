import matplotlib.pyplot as plt
import numpy as np
import math
import sys
from scipy import interpolate
import scan.cxSM as cxSM
from higgs_decay_width_new import *
import fileinput
import os
i=int(sys.argv[1])

input_file=[]
for n in range(40):
    input_file.append(os.path.join('output',str(n),'result.txt'))
for n in range(100):
    input_file.append(os.path.join('first_2',str(n),'result.txt'))
for n in range(100):
    input_file.append(os.path.join('first_3',str(n),'result.txt'))

data=np.loadtxt(input_file[i])
for ii in range(len(data)):
    omega=data[ii,14]
    strength=data[ii,7]
    mp=938.2720813*0.001
    v0=246.0
    a1=data[ii,0]
    x0=data[ii,1]
    s1=data[ii,2]
    c1=np.sqrt(1-s1**2)
    mh=data[ii,3]
    ms=data[ii,4]
    mA=data[ii,5]
    order=data[ii,6]
    mh2=mh**2
    ms2=ms**2
    mA2=mA**2
    delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
    d2=2*(np.sqrt(2)*a1+ms2*x0*c1*c1+mh2*x0*s1*s1)/x0**3
    lam=2*(ms2*c1*c1+mh2*s1*s1)/x0**2
    tan=x0/v0
    tmp=lam+2*delta2*tan**2+d2*tan**4

    output=open("check_strength.txt", mode='a+')

    output.write("{}  {}  {}  {}  {}\n".format(order,strength,delta2,d2,tmp))
    output.flush()