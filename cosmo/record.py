import numpy as np
import math
import sys
from scipy import interpolate
import scan.cxSM as cxSM
from higgs_decay_width_new import *
import fileinput
import os
i=int(sys.argv[1])

#input_file=['/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/output/40/result.txt','/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/output/41/result.txt']
#input_file=['output/1/result.txt']
#for n in range(2500):
    #input_file.append(os.path.join('first_2',str(n),'result.txt'))
#for n in range(2500):
    #input_file.append(os.path.join('first_3',str(n),'result.txt'))
input_file=[]
for n in range(100):
    input_file.append(os.path.join('try4',str(n),'result.txt'))

data=np.loadtxt(input_file[i])

omega=data[:,14]
strength=data[:,7]
mp=938.2720813*0.001
v0=246.0*np.ones(len(data))
a1=data[:,0]
x0=data[:,1]
s1=data[:,2]
c1=np.sqrt(1-s1**2)
mh=data[:,3]
ms=data[:,4]
mA=data[:,5]
order=data[:,6]
mh2=mh**2
ms2=ms**2
mA2=mA**2
delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
y=data[:,8]*10**(-36)
y=y*omega/0.1196

output=open("record.txt", mode='a+')
output1=open("record_pre.txt", mode='a+')

ary_a1=np.array(a1); ary_x0=np.array(x0); ary_v0=np.array(v0); ary_s1=np.array(s1)
ary_mh2=np.array(mh2); ary_ms2=np.array(ms2); ary_mA2=np.array(mA2)

cxSM_decay_class = cxSM_decay_width(ary_a1, ary_x0, ary_v0, ary_s1, ary_mh2, ary_ms2, ary_mA2)
cro_sec_h2_1 = cxSM_decay_class.cro_sec_2body_tmp()
cro_sec_h2_2 = cxSM_decay_class.cro_sec_2body()
cro_sec_h2_3 = cxSM_decay_class.cro_sec_3body()
br_h_AA = cxSM_decay_class.BR_h1_AA()
br_h_hAA = cxSM_decay_class.BR_h2_h1AA()
cro_sec_h2_h1h1 = cxSM_decay_class.xsec_h2_h1h1()
bbtt = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbtt.txt', delimiter="\t", header=None)
bbtt_func = interpolate.interp1d(bbtt.iloc[:,0].values,bbtt.iloc[:,1].values,kind='linear')
bbbb = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbbb.txt', delimiter="\t", header=None)
bbbb_func = interpolate.interp1d(bbbb.iloc[:,0].values,bbbb.iloc[:,1].values,kind='linear')
bbyy = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbyy.txt', delimiter="\t", header=None)
bbyy_func = interpolate.interp1d(bbyy.iloc[:,0].values,bbyy.iloc[:,1].values,kind='linear')
cro_sec_h2_VV = cxSM_decay_class.xsec_h2_VV()
vv = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/vbf_VV.txt', delimiter="\t", header=None)
vv_func = interpolate.interp1d(vv.iloc[:,0].values*1000,vv.iloc[:,1].values,kind='linear')

for ii in range(len(ms)):
    if ((cro_sec_h2_2[ii]>0.0) and (s1[ii]>-0.274) and (s1[ii]<0.274) and (y[ii]<5.99e-47) and (omega[ii]<0.1196) and (cro_sec_h2_h1h1[ii]< bbtt_func(ms[ii])) and (cro_sec_h2_h1h1[ii]< bbbb_func(ms[ii])) and (cro_sec_h2_h1h1[ii]< bbyy_func(ms[ii])) and (cro_sec_h2_VV[ii]< vv_func(ms[ii]))):
        output.write("2  "+"{}  {}  {}\n".format(ms[ii],0.019*cro_sec_h2_1[ii],strength[ii]))
        output.flush()
    if ((cro_sec_h2_3[ii]>0.0) and (s1[ii]>-0.274) and (s1[ii]<0.274) and (y[ii]<1.30e-46) and (omega[ii]<0.1196) and (cro_sec_h2_h1h1[ii]< bbtt_func(ms[ii])) and (cro_sec_h2_h1h1[ii]< bbbb_func(ms[ii])) and (cro_sec_h2_h1h1[ii]< bbyy_func(ms[ii])) and (cro_sec_h2_VV[ii]< vv_func(ms[ii]))):
        output.write("3  "+"{}  {}  {}\n".format(ms[ii],cro_sec_h2_3[ii],strength[ii]))
        output.flush()
    if (cro_sec_h2_2[ii]>0.0):
        output1.write("1  "+"{}  {}  {}\n".format(ms[ii],cro_sec_h2_1[ii],strength[ii]))
        output.flush()
    if (cro_sec_h2_3[ii]>0.0):
        output1.write("2  "+"{}  {}  {}\n".format(ms[ii],cro_sec_h2_3[ii],strength[ii]))
        output.flush()
        output1.write("3  "+"{}  {}  {}\n".format(ms[ii],br_h_hAA[ii],strength[ii]))
        output.flush()