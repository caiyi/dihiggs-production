import numpy as np
from higgs_decay_width import *
import matplotlib.pyplot as plt

ms = np.arange(300, 1000, 0.1)
ms2 = ms**2
v=246.0*np.ones(len(ms))
x0=40.*np.ones(len(ms))
sin=-0.2*np.ones(len(ms))
mh2=125.**2*np.ones(len(ms))
mA2=62.49**2*np.ones(len(ms))
a_1=-1000000.*np.ones(len(ms))

cxSM_decay_class_1 = cxSM_decay_width(a_1,x0,v,sin,mh2,ms2,mA2)
y = cxSM_decay_class_1.cro_sec_2body_tmp()*0.019
z = cxSM_decay_class_1.cro_sec_3body()

fig,ax=plt.subplots(figsize=(8,8), dpi=80)
ax.plot(ms, y)
ax.plot(ms, z)
ax.set_yscale('log')
ax.set_ylim(10e-6, 10e0)
ax.set_xlabel("mS {GeV}")
ax.set_ylabel("$\sigma$")
fig.savefig("tmp.png")