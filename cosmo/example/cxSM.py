import numpy as np
import math
from scipy import linalg
from cosmoTransitions.generic_potential import generic_potential
		
	
class ComplexSingletSM(generic_potential):
	def init(self, x0, lmda, mu2, delta2, a1, b1, b2, d2):
		self.Ndim = 2
		
		self.v0 = 246.221
		self.x0, self.lmda, self.mu2, self.delta2, self.a1, self.b1, self.b2, self.d2 = x0*1.0, lmda*1.0, mu2*1.0, delta2*1.0, a1*1.0, b1*1.0, b2*1.0, d2*1.0
		#self.lmda, self.mu2, self.b2, self.b3 = linalg.solve(A,B)
				
		# Do checks to ensure that the potential is bounded
		assert self.d2 > 0
		assert self.lmda > 0
		###assert self.delta2**2-self.lmda*self.d2 > 0
		# Check that the minimum is actually a minimum
		#assert Mhh > 0
		#assert Mss > 0
		#assert Mhh*Mss - v0**2*( 0.5*self.a1 + self.a2*x0 )**2 > 0
		
		# Set the yukawa couplings and the gauge couplings.
		self.g = 2 * 80.385 / 246.0 # mW = g*v/2
		sqrt_ggp = 2 * 91.1876 / 246.0 # mZ = sqrt(g*g+gp*gp) * v/2
		self.gp = np.sqrt(sqrt_ggp*sqrt_ggp - self.g*self.g)
		self.yt = np.sqrt(2) * 173.03 / 246.0 # mt = yt*v/sqrt(2)
		
		self.Tmax = 3000. # 10000 was causing problems.
		self.forbidPhaseCrit = lambda X: True if (X[0] < -5.0) else False  # don't let h go too negative
		
#	def getPhases(self,startHigh=True,**tracingArgs):
		# We'll get screwy results using the oringal method if there's a tree-level barrier,
		# since it uses T0 to set the scale. 
		# Instead, just use mu and b2 as the scale.
		#T0_old = self.T0 if self.T0 != None else self.findT0() # used as the characteristic T scale
		#self.T0 = max(abs(self.mu2), abs(self.b2), self.T0**2)**0.5
		#generic_potential.getPhases(self, startHigh, **tracingArgs)
		#self.T0 = T0_old # set T0 to its old value
		
	def V0(self, X):
		X = np.array(X)
		h2,s = X[...,0]**2, X[...,1]
		y = -0.25*self.mu2*h2 + 0.25*0.25*self.lmda*h2*h2
		y += 0.25*self.b2*s**2 + 0.25*0.5*self.delta2*h2*s**2 + 0.25*0.25*self.d2*s**4
		y += 2.0*(self.a1*s/math.sqrt(2) + 0.25*0.5*self.b1*s**2)
		return y
		
	def scalarMassSqs(self, v0, x):
		# M is the mass matrix. h is higgs direction, s is singlet, g is goldstone
		MAA = -math.sqrt(2)*self.a1/x-self.b1
		Mhh = 0.5*self.lmda*v0**2
		Mss = 0.5*self.d2*x**2 - math.sqrt(2)*self.a1/x
		Mhs = 0.5*v0*x*self.delta2
		return MAA, Mhh, Mss, Mhs
		
	def Vtot(self, X, T, include_radiation=True):
		X = np.array(X)
		h,s = X[...,0], X[...,1]
		y = self.V0(X)
		# Add in thermal mass contributions
		# For bosons, this is just T^2*m^2 / 24
		# For fermions, it's T^2*m^2 / 48
		MAA, Mhh, Mss, Mhs = self.scalarMassSqs(h,s)
		mW2 = 0.25*self.g*self.g*h*h
		mZ2 = mW2 + 0.25*self.gp*self.gp*h*h
		y += T*T * (Mhh + Mss + MAA + 6*mW2 + 3*mZ2) / 24.
		mt2 = 0.5*self.yt*self.yt*h*h
		y += T*T * 12*mt2 / 48.
		return y

	def V1T_from_X(self, X, T, include_radiation=True):
		# This is called by dgradV_dT. It's supposed to return just the
		# temperature-dependent part of the potential, but it's fine if it 
		# returns the whole thing.
		# Since we're overriding Vtot, we need to override this too.
		return self.Vtot(X, T, include_radiation)
						
	def approxZeroTMin(self):
		# might want to add in more minima here if we can figure them out analytically.
		m = [ np.array([self.v0, self.x0]) ]
		if (self.mu2 > 0 and self.b2 > 0):
			m.append(m[0]*1e-4) # add the origin as a minimum
			# (adding something very close to the origin rather than the exact origin so that
			# the tracing algorithm doesn't get stuck there)
		return m
		
