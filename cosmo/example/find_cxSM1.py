import numpy as np
import math
import pickle
import random
import cxSM



f=open('./test_cxSM.dat', 'a+')
for i in range(1,100000):
       mh2=random.uniform(120.0**2, 130.0**2); ms2=random.uniform(65.0**2,750.0**2); mA2=random.uniform(65.0**2, 2000.0**2);
       x0=random.uniform(0.0, 150.0); s1=random.uniform(0.01,0.3); a1=random.uniform(-100.0**3, 100.0**3);

       c1=math.sqrt(1-s1**2); v0=246.0;
       lam=(2*(mh2*c1**2+ms2*s1**2))/v0**2; delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0); d2=(2*math.sqrt(2)*a1+ms2*x0*c1**2+mh2*x0*s1**2)/(x0**3);
       b1=(-2*math.sqrt(2)*a1-mA2*x0)/(2*x0);
       mu2=1.0/2.0*(-x0**2*delta2-v0**2*lam); sum12=(-4*math.sqrt(2)*a1-d2*x0**3-v0**2*x0*delta2)/(2*x0);
       b2=sum12-b1;

       ###print('lam=', lam, 'mu2=', mu2, 'delta2=', delta2, 'b2=', b2, 'd2=', d2, 'a1=', a1, 'b1=', b1)
       if not(delta2**2-lam*d2 < 0 and d2 > 0 and lam > 0):
          continue

       model = cxSM.ComplexSingletSM(x0, lam, mu2, delta2, a1, b1, b2, d2)
       ###print('**'*20)
       try:
          phases = model.getPhases()
          tctrans = model.calcTcTrans()
       except:
          continue
       else:
          print('**'*20)
          EW_vac_phase = None
          phase_diff = np.inf
          for key, phase in phases.items():
              vev = phase.valAt(T=0.0)
              if abs(vev[0] - v0) < phase_diff:
                  phase_diff = abs(vev[0] - v0)
                  EW_vac_phase = key

          for trans in tctrans:
              if trans['low_phase'] == EW_vac_phase and trans['trantype'] == 1.0:
                 try:
                    alltran = model.findAllTransitions()
                 except:
                    continue
                 else:
                    alltrans = [x for x in alltran if x is not None]
                    if len(alltrans) > 0:
                       trans_dict = {}
                       for trans_count, trans in enumerate(alltrans):
                           trans_dict = {'Critical Temperature': trans['crit_trans']['Tcrit'],
                           'Nucleation Temperature': trans['Tnuc'],
                           'Action': trans['action'],
                           'criT Lvev1': trans['crit_trans']['low_vev'][0],
                           'criT Lvev2': trans['crit_trans']['low_vev'][1],
                           'criT Hvev1': trans['crit_trans']['high_vev'][0],
                           'criT Hvev2': trans['crit_trans']['high_vev'][1],
                           'Trtype': trans['trantype'],
                           'phi1 High VEV': trans['high_vev'][0],
                           'phi1 Low VEV': trans['low_vev'][0],
                           'phi2 High VEV': trans['high_vev'][1],
                           'phi2 Low VEV': trans['low_vev'][1]}
                           phic_Tc=math.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2+(trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']
                           Hc_Tc=math.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2)/trans_dict['Critical Temperature']
                           sc_Tc=math.sqrt((trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']
                           f.write(str(s1)+'  '+str(g_211)+'  '+str(ms)+'  '+str(mhiggs)+'  '+str(Br_h2ZZ)+'  '+str(Br_h211)+'  '+str(x1)+'  '+str(x2)+'  '+str(x3)+'  '+str(x4)+'  '+str(x5)+'  '+str(x6)+'  '+str(x7)+'  '+str(CS_pp_h2_h1h1)+'  '+str(CS_pp_h2_ZZ)+'  '+str(CS_pp_h2_h1h1_bbyy)+'  '+str(CS_pp_h2_ZZ_llll)+'  '+str(CS_bbyy_aft2bCUT)+'  '+str(CS_bbyy_aft1bCUT)+'  '+str(trans_dict['Critical Temperature'])+'  '+str(trans['trantype'])+'  '+str(trans_dict['Nucleation Temperature'])+'  '+str(trans_dict['criT Lvev1'])+'  '+str(trans_dict['criT Lvev2'])+'  '+str(trans_dict['criT Hvev1'])+'  '+str(trans_dict['criT Hvev2'])+'  '+str(trans_dict['Trtype'])+'\n')
                           f.flush()

























