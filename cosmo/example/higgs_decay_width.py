import numpy as np
import math
import pickle
import random
import xSM
import sys
import pandas as pd
from scipy import interpolate
from matplotlib.pylab import plt
from scipy import linalg as la


df_br=pd.read_csv('BR.csv', sep=',',header=None)
df_xsec14 = pd.read_csv('XS14TEV.txt', delimiter="\t", header=None)
#interpolated ggH cross-section in pb at 14TeV 
Xsec14_ggH = interpolate.interp1d(df_xsec14.iloc[:,0].values,df_xsec14.iloc[:,1].values)

MH_list = df_br.iloc[:, 0].values
total_Width_list = df_br.iloc[:,-3].values

#created interpolated function for the decay width of SM_like Higgs of different mass
channel_list = ["bb","tautau","mumu","cc","ss","tt","gg","yy","Zy","WW","ZZ"]
interWidth = {}
iter = 1
for channel in channel_list:
    tempBr_list = df_br.iloc[:,iter].values
    tempWidth_list = total_Width_list*tempBr_list
    interWidth[channel] = interpolate.interp1d(MH_list,tempWidth_list)
    iter =  iter + 3
interWidth["total"] = interpolate.interp1d(MH_list,total_Width_list)

#function return the Branching ratio of the heavy Higgs in xSM:
#usage example: HBR("bb", 500., 0.001, 0.01)
def HBR(channel, mH, sintheta, Width_H2h):
    total_width = interWidth["total"](mH)*sintheta**2 + Width_H2h
    print(interWidth["total"](mH))
    if channel == "hh":
        return Width_H2h/total_width
    else:
        return interWidth[channel](mH)*sintheta**2/total_width

def XS14TEVggH(mH, sintheta):
    return Xsec14_ggH(mH)*sintheta**2



##Nc is the color quantum number.
def h_ff(mh, mf, Nc, vev):
    y=Nc/(8*math.pi)*(mf**2/vev**2)*mh*pow((1-4*mf**2/mh**2),3.0/2.0)
    return y


##For W boson, Nc=2. For Z boson, Nc=1.
def h_AA(mh, mA, Nc, vev):
    y=Nc/(8*math.pi)*(pow(mA,4)/(mh*vev**2))*(pow((1-4*mA**2/mh**2),1.0/2.0))*(3+1.0/4.0*pow(mh/mA,4)-mh**2/mA**2)
    return y



###Consider top quark loop
def Gamma_LO(m_ratio):
    y=1+7.0/15.0*m_ratio+1543.0/6000.0*m_ratio**2+226.0/1575.0*pow(m_ratio,3)
    return y


def h_gg(mh, mt):
    alpha_s=1; vev=246.0;
    Gf=math.sqrt(2)/(2*vev**2)
    y=Gf*pow(mh,3)/(36*math.sqrt(2)*math.pi)*pow(alpha_s/math.pi,2)*Gamma_LO(mh**2/(4*mt**2))
    #y=pow(mh,3)/(8*math.pi*vev**2)*pow(alpha_s/math.pi,2)*pow(m/mh,4)*Dn(pow(m/mh,2))
    return y


###Total decay width
def h_SM(mh, vev):
    mb=4.18; mtau=1.777*pow(10,-3); mc=1.275; mW=80.379; mZ=91.1876; mt=173.0;
    h_width=h_ff(mh, mb, 3, vev)+h_ff(mh, mc, 3, vev)+h_ff(mh, mtau, 1, vev)+h_AA(mh, mW, 2, vev)+h_AA(mh, mZ, 1, vev)+h_gg(mh, mt)
    return h_width




###Decay width of h2->h1 h1
def h2_h1h1(g_211, mh2, mh1):
    y=g_211**2*math.sqrt(1-4*mh1**2/mh2**2)/(8*math.pi*mh2)
    return y


###Branch ratio
def Br_h2_h1h1(g_211, mh2, mh1, vev, sin_theta):
    br=h2_h1h1(g_211, mh2, mh1)/(h2_h1h1(g_211, mh2, mh1)+sin_theta**2*h_SM(mh2, vev))
    return br



def Br_h2_ZZ(g_211, mh2, mh1, vev, sin_theta):
    mZ=91.1876;
    br=(sin_theta**2*h_AA(mh2, mZ, 1, vev))/(h2_h1h1(g_211, mh2, mh1)+sin_theta**2*h_SM(mh2, vev))
    return br



def Br_h2_h1h1_bbyy(g_211, mh2, mh1, vev, sin_theta):
    h1_bb=0.577; h1_yy=2.28*pow(10,-3);
    br=Br_h2_h1h1(g_211, mh2, mh1, vev, sin_theta)*2*h1_bb*h1_yy;
    return br



def Br_h2_ZZ_llll(g_211, mh2, mh1, vev, sin_theta):
    Z_ll=3.3658*pow(10,-2);
    br=Br_h2_ZZ(g_211, mh2, mh1, vev, sin_theta)*2*Z_ll*Z_ll
    return br



def eff_pp_h2_ZZ_4l(mh):
    y=0.255198 + 0.000638337*mh - 6.04309*pow(10,-7)*pow(mh,2) + 1.93095*pow(10,-10)*pow(mh,3)
    return y



def eff_bbyy_2btag(mh):
    if mh<400.0:
       y=0.103661 - 0.000480608*mh + 1.477*pow(10,-6)*mh**2 - 1.05908*pow(10,-9)*pow(mh,3)
    else:
       y=-0.284147 + 0.00116173*mh - 1.05085*pow(10,-6)*mh**2 + 3.2726*pow(10,-10)*pow(mh,3)
    return y




def eff_bbyy_1btag(mh):
    if mh<400.0:
       y=0.0372772 - 0.000219921*mh + 1.75063*pow(10,-6)*mh**2 - 2.70001*pow(10,-9)*pow(mh,3)
    else:
       y=-0.131216 + 0.000551814*mh - 4.84208*pow(10,-7)*mh**2 + 1.8661*pow(10,-10)*pow(mh,3)
    return y


    




def Mhiggs(a1,a2,b3,b4,lam,v0,x0):
    rst=[]
    US2 = -((a1*v0**2)/(4*x0)) + x0*(b3 + 2*b4*x0)
    UH2 = 2*lam*v0**2;
    USH = v0*(a1 + 2*a2*x0)/2.0
    t2 = -(2*USH)/(US2 - UH2)
    x = t2
    t1 = 1/x*(-1 + math.sqrt(1 + x**2))
    
    c1 = 1/math.sqrt(t1**2 + 1)
    s1 = t1/math.sqrt(t1**2 + 1)
    try:
        ###ms = math.sqrt( US2*c1**2 + UH2*s1**2 - USH*2*c1*s1 )
        ###mhiggs = math.sqrt( US2*s1**2 + UH2*c1**2 + USH*2*c1*s1 )
        Mass_matrix=np.array([[UH2,USH],[USH,US2]])
        eigval,eigvec=la.eig(Mass_matrix)
        eigval=eigval.real
        mh2=eigval[0]
        ms2=eigval[1]
        mhiggs=math.sqrt(mh2); ms=math.sqrt(ms2);
        ###print('mh2=', mh2, '  ms2=', ms2)
    except: 
        return rst;
    ###ms = math.sqrt(( US2*s1**2 + UH2*c1**2 - USH*2*c1*s1 ))
    ###Mass_matrix=np.array([[UH2,USH],[USH,US2]])
    ###eigval,eigvec=la.eig(Mass_matrix)
    ###eigval=eigval.real
    ###mh2=eigval[0]; ms2=eigval[1];
    mhiggs=math.sqrt(mh2)
    ms=math.sqrt(ms2)
    g_211=0.5*(-6*lam*v0*c1**2*s1 + 2*b3*c1*s1**2 + 6*b4*x0*c1*s1**2 + 0.25*a1*(2*c1**3-4*c1*s1**2) + 0.25*a2*(4*x0*c1**3 + 8*v0*c1**2*s1 -8*x0*c1*s1**2 - 4*v0*s1**3))
    ###print('mhigss=',mhiggs,'  ms=',ms,'  g_211=',g_211)
    rst.append(s1)
    rst.append(g_211)
    rst.append(ms)
    rst.append(mhiggs)
    rst.append(c1)
    return rst




class cxSM_decay_width(object):

      def __init__(self, a1, vs, v0, sin_theta, m12, m22, mA2):
          self.a1=a1; self.vs=vs; self.v0=v0; self.sin_theta=sin_theta; self.m12=m12; self.m22=m22; self.mA2=mA2
          self.cos_theta=math.sqrt(1-sin_theta**2)
          self.sin_2theta=2*sin_theta*self.cos_theta
          self.m1=math.sqrt(m12); self.m2=math.sqrt(m22); self.mA=math.sqrt(mA2);
          self.higgs_like_decay_width=interWidth["total"](self.m2);
          self.higgs_SM_decay_width=interWidth["total"](self.m1);
          self.h2_produce_cro_sec=XS14TEVggH(self.m2, sin_theta)

      def cxSM_coupling(self):
          cos_theta=self.cos_theta; a1=self.a1; vs=self.vs; v0=self.v0; sin_theta=self.sin_theta; m12=self.m12; m22=self.m22;
          sin_2theta=self.sin_2theta
          g_111=0.5*(m12*cos_theta**3/v0+(math.sqrt(2)*a1+m12*vs)*sin_theta**3/(vs**2))
          g_211=(3*math.sqrt(2)*a1*v0*sin_theta+(2*m12+m22)*vs*(v0*sin_theta-vs*cos_theta))*sin_2theta/(4*v0*vs**2)
          g_1AA=(math.sqrt(2)*a1+m12*vs)*sin_theta/(2*vs**2)
          g_2AA=(math.sqrt(2)*a1+m22*vs)*cos_theta/(2*vs**2)
 
          return g_111, g_211, g_1AA, g_2AA

      def h2_h1h1_decay_width(self):
          m12, m22, m2, m1 = self.m12, self.m22, self.m2, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          if m1 > 2*m2:
             Gamma = g_211**2*math.sqrt(1-4*m12/m22)/(8*math.pi*m2) 
             return Gamma
          else:
             return 0.0

      def h1_AA_decay_width(self):
          m12, mA2, mA, m1 = self.m12, self.mA2, self.mA, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          if m1 > 2*mA:
             Gamma = g_1AA**2*math.sqrt(1-4*mA2/m12)/(8*math.pi*m1)   
             return Gamma
          else:
             return 0.0

      def h2_AA_decay_width(self):
          m22, mA2, mA, m2 = self.m22, self.mA2, self.mA, self.m2
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          if m2 > 2*mA:
             Gamma = g_2AA**2*math.sqrt(1-4*mA2/m22)/(8*math.pi*m2)
             return Gamma
          else:
             return 0.0

      def BR_h2_h1h1(self):
          h2_h1h1_dw = self.h2_h1h1_decay_width(); h2_AA_dw = self.h2_AA_decay_width()
          total_h1_decay_width = self.sin_theta**2*self.higgs_like_decay_width + h2_h1h1_dw + h2_AA_dw
          br_h2_h1h1 = h2_h1h1_dw/total_h1_decay_width
          return br_h2_h1h1

      def cro_sec_h2_h1h1(self):
          total_cro_sec = self.h2_produce_cro_sec     
          br_h2_h1h1 = self.BR_h2_h1h1()   
          return br_h2_h1h1*total_cro_sec    

      def BR_h1_AA(self):
          h1_AA_dw = self.h1_AA_decay_width()
          total_h1_decay_width = self.cos_theta**2*self.higgs_SM_decay_width + h1_AA_dw
          br_h1_AA = h1_AA_dw/total_h1_decay_width
          return br_h1_AA

      ###This gives the cross section of p p > h2 > h1 h1, h1 > SM SM, h1 > A A
      def cro_sec_final(self):
          br_h2_h1h1 = self.BR_h2_h1h1(); br_h1_AA = self.BR_h1_AA();
          cro_sec = self.h2_produce_cro_sec*br_h2_h1h1*br_h1_AA
          return cro_sec







