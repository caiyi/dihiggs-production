import numpy as np
import math
import pickle
import random
import xSM_v2
import sys
#sys.path.append("/home/zhangwenxing/zhangwx/code/EWPT/Wenxing/benchmark/search/xSM_CosmoTranstions_Scans")
from higgs_decay_width import *



f=open('./mediumM7.dat', 'a+')

for i in range(1,10):
    x1=random.uniform(-932.0, -712.0)
    x2=random.uniform(3.43, 5.63)
    x3=random.uniform(-293.0, -103.0)
    x4=random.uniform(0.37, 0.77)
    x5=random.uniform(0.10, 0.34)
    x6=246.0
    x7=random.uniform(19.2, 59.2)
    result=Mhiggs(x1, x2, x3, x4, x5, x6, x7)
    if len(result)==0:
       continue

    s1=result[0]; g_211=result[1]; ms=result[2]; mhiggs=result[3];
    a1,a2,b3,b4,lam,v0,x0=x1, x2, x3, x4, x5, x6, x7
    mu2=lam*v0**2+(a1+a2*x0)*x0/2.0
    b2=-b3*x0-b4*x0**2-(a1*v0**2)/(4*x0)-(a2*v0**2)/2.0
       
    if abs(result[3]-125.0)<5.0 or abs(result[2]-125.0)<5.0:
       s1=result[0]; g_211=result[1]; ms=result[2]; mhiggs=result[3]; c1=result[4];
       a1,a2,b3,b4,lam,v0,x0=x1, x2, x3, x4, x5, x6, x7
       mu2=lam*v0**2+(a1+a2*x0)*x0/2.0
       b2=-b3*x0-b4*x0**2-(a1*v0**2)/(4*x0)-(a2*v0**2)/2.0
       if abs(result[3]-125.0)<5.0 and result[2]>2*result[3]:
          mhiggs=result[3]; ms=result[2]
       elif abs(result[2]-125.0)<5.0 and result[3]>2*result[2]:
          mhiggs=result[2]; ms=result[3]
       else:
          continue
       CS_pp_h2=XS14TEVggH(ms, s1)

       width_h211=h2_h1h1(g_211, ms, mhiggs)
       Br_h2ZZ=HBR("ZZ", ms, s1, width_h211)
       Br_h211=HBR("hh", ms, s1, width_h211)
       
       CS_pp_h2_h1h1=CS_pp_h2*Br_h211;   CS_pp_h2_ZZ=CS_pp_h2*Br_h2ZZ;
       h1_bb=0.577; h1_yy=2.28*pow(10,-3); Z_ll=3.3658*pow(10,-2);
       h1_bb=c1**2*0.577; h1_yy=c1**2*2.28*pow(10,-3); Z_ll=3.3658*pow(10,-2);
       CS_pp_h2_h1h1_bbyy = CS_pp_h2_h1h1*2*h1_bb*h1_yy;   CS_pp_h2_ZZ_llll = CS_pp_h2_ZZ*2*Z_ll*Z_ll;
       CS_bbyy_aft2bCUT=CS_pp_h2_h1h1_bbyy*eff_bbyy_2btag(ms); CS_bbyy_aft1bCUT=CS_pp_h2_h1h1_bbyy*eff_bbyy_1btag(ms);
       ##Br_h2ZZ=Br_h2_ZZ(g_211, ms, mhiggs, v0, s1)
       ##Br_h211=Br_h2_h1h1(g_211, ms, mhiggs, v0, s1)

       v0=246.0
       model = xSM.singletSM(x0, lam, mu2, a1, a2, b2, b3, b4)
       try:
          phases = model.getPhases()
          tctrans = model.calcTcTrans()
       except:
          continue
       else:
          EW_vac_phase = None
          phase_diff = np.inf
          for key, phase in phases.iteritems():
              vev = phase.valAt(T=0.0)
              if abs(vev[0] - v0) < phase_diff:
                  phase_diff = abs(vev[0] - v0)
                  EW_vac_phase = key

          for trans in tctrans:
              if trans['low_phase'] == EW_vac_phase and trans['trantype'] == 1.0:
                 try:
                    alltran = model.findAllTransitions()
                 except:
                    continue
                 else:
                    alltrans = [x for x in alltran if x is not None]
                    if len(alltrans) > 0:
                       trans_dict = {}
                       for trans_count, trans in enumerate(alltrans):
                           trans_dict = {'Critical Temperature': trans['crit_trans']['Tcrit'],
                           'Nucleation Temperature': trans['Tnuc'],
                           'Action': trans['action'],
                           'criT Lvev1': trans['crit_trans']['low_vev'][0],
                           'criT Lvev2': trans['crit_trans']['low_vev'][1],
                           'criT Hvev1': trans['crit_trans']['high_vev'][0],
                           'criT Hvev2': trans['crit_trans']['high_vev'][1],
                           'Trtype': trans['trantype'],
                           'phi1 High VEV': trans['high_vev'][0],
                           'phi1 Low VEV': trans['low_vev'][0],
                           'phi2 High VEV': trans['high_vev'][1],
                           'phi2 Low VEV': trans['low_vev'][1]}
                           phic_Tc=math.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2+(trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']
                           Hc_Tc=math.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2)/trans_dict['Critical Temperature']
                           sc_Tc=math.sqrt((trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']    
                           f.write(str(s1)+'  '+str(g_211)+'  '+str(ms)+'  '+str(mhiggs)+'  '+str(Br_h2ZZ)+'  '+str(Br_h211)+'  '+str(x1)+'  '+str(x2)+'  '+str(x3)+'  '+str(x4)+'  '+str(x5)+'  '+str(x6)+'  '+str(x7)+'  '+str(CS_pp_h2_h1h1)+'  '+str(CS_pp_h2_ZZ)+'  '+str(CS_pp_h2_h1h1_bbyy)+'  '+str(CS_pp_h2_ZZ_llll)+'  '+str(CS_bbyy_aft2bCUT)+'  '+str(CS_bbyy_aft1bCUT)+'  '+str(trans_dict['Critical Temperature'])+'  '+str(trans['trantype'])+'  '+str(trans_dict['Nucleation Temperature'])+'  '+str(trans_dict['criT Lvev1'])+'  '+str(trans_dict['criT Lvev2'])+'  '+str(trans_dict['criT Hvev1'])+'  '+str(trans_dict['criT Hvev2'])+'  '+str(trans_dict['Trtype'])+'\n')
                           f.flush()

                 ###   f.write(str(s1)+'  '+str(g_211)+'  '+str(ms)+'  '+str(mhiggs)+'  '+str(x1)+'  '+str(x2)+'  '+str(x3)+'  '+str(x4)+'  '+str(x5)+'  '+str(x6)+'  '+str(x7)+'  '+str(trans_dict['Critical Temperature'])+'  '+str(alltrans[0]['trantype'])+'  'str(alltrans[1]['trantype'])+'  '+str(trans_dict['Nucleation Temperature'])+'  '+str(trans_dict['Action'])+'  '+str(trans_dict['phi1 High VEV'])+'  '+str(trans_dict['phi1 Low VEV'])+'  '+str(trans_dict['phi2 High VEV'])+'  '+str(trans_dict['phi2 Low VEV'])+'\n')



f.close()





















