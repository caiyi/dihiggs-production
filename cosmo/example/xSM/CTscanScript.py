import xSM
import sys
#from numpy import *
import numpy as np

#Upload the result of the MC scan over the param space into an array
data = np.genfromtxt("LHCResultsCT.txt")



#Create an array to hold param points with the info about any/all transitions
MCpoints = []
getPhasesErrorPoints = []
AllTransErrorPoints = []

#Sum over all points in the data array. However, until everything is up and working, just use the first few. Eventually we'll sum up to len(data).
for i in range(0,len(data)):
   
#Upload param point info
    v0 = 246.221
    cos, m2, x0, lmda, mu2, a1, a2, b2, b3, b4 = data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6], data[i][7], data[i][8], data[i][9]

#Create an instance of the model
    model = xSM.singletSM(x0, lmda, mu2, a1, a2, b2, b3, b4)

#Print out the percentage of points that have been read in
    print (i, 1-np.true_divide( i, len(data) ) )

#It takes too long to gather full transition info about every transition that occurs as most won't be of interest. Instead, gather fast info which allows
#us to quickly discern the presence of any transitions which ended in the EW phase. We'll only gather full transition info from these.

#Although rare, some points are too finely tuned for numerical differential equation solvers. These points throw various different exceptions. 
#Keep exception info but simply continue because these are simply finely tuned and not representative of the generic regions which we're interested in.
    try:
        phases = model.getPhases()
        tctrans = model.calcTcTrans()
    except:
        exc_type = sys.exc_info()[0]
        getPhasesErrorPoints.append([exc_type, i])
        np.savetxt('getPhasesErrorLog.txt', getPhasesErrorPoints, fmt='%s')
        continue
    else:

#Identify the key for the phase that is closest to the EW vacuum
        EW_vac_phase = None
        phase_diff = np.inf
        for key, phase in phases.iteritems():
            vev = phase.valAt(T=0.0)
            if abs(vev[0] - v0) < phase_diff:
                phase_diff = abs(vev[0] - v0)
                EW_vac_phase = key

#Gather full info about any 1st order transition that ended in the EW phase 
        for trans in tctrans:
            if trans['low_phase'] == EW_vac_phase and trans['trantype'] == 1.0:
                try: 
                    AllTrans = model.findAllTransitions()
                except:
                    exc_type = sys.exc_info()[0]
                    AllTransErrorPoints.append([exc_type, i])
                    np.savetxt('AllTransErrorLog.txt', AllTransErrorPoints, fmt='%s')
                    continue
                else:
                    if len(AllTrans) == 0:
                        continue
                    else:
                        for fullTrans in AllTrans:
                            if fullTrans['low_phase'] == EW_vac_phase:
                                MCpoints.append((cos, m2, x0, lmda, mu2, a1, a2, b2, b3, b4, fullTrans['crit_trans']['low_vev'][0], fullTrans['crit_trans']['high_vev'][0], fullTrans['crit_trans']['low_vev'][1], fullTrans['crit_trans']['high_vev'][1], fullTrans['crit_trans']['Tcrit'], fullTrans['Tnuc'], fullTrans['action'], fullTrans['trantype']))
                                np.savetxt('1stOrderPoints.dat', MCpoints, fmt='%s')
