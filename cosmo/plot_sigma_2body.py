import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from higgs_decay_width import *
import os
import fileinput
input_file=[]
for n in range(500):
    input_file.append(os.path.join('full_output',str(n),'result.txt'))
data=np.loadtxt(fileinput.input(input_file))
df_1 = np.loadtxt("XENON1T.csv")
df_n = np.loadtxt("XENONnT.csv")
x_1=df_1[:,0]
y_1=df_1[:,1]
x_n=df_n[:,0]
y_n=df_n[:,1]
y_1_625=(6.024254966711994e-47-5.97925295088131e-47)*0.15+5.97925295088131e-47
y_n_625=(1.8439100598840942e-48-1.843589949484231e-48)/4+1.843589949484231e-48
x_1_=[60,1000]
y_1_=[y_1_625,y_1_625]
x_n_=[60,1000]
y_n_=[y_n_625,y_n_625]

color=[]
omega=data[:,14]
strength=data[:,7]
mp=938.2720813*0.001
v=246.0*np.ones(len(data))
a1=data[:,0]
vs=data[:,1]
sin=data[:,2]
cos=np.sqrt(1-sin**2)
mh=data[:,3]
ms=data[:,4]
mA=data[:,5]
order=data[:,6]
mh2=mh**2
ms2=ms**2
mA2=mA**2
delta2=(2*(mh2-ms2)*cos*sin)/(vs*v)
index1=[]
index2=[]
index3=[]
index4=[]
'''
cxSM_decay_class = cxSM_decay_width(a1,vs,v,sin,mh2,ms2,mA2)
g_111, g_211, g_1AA, g_2AA = cxSM_decay_class.cxSM_coupling()
'''
g_1AA=(np.sqrt(2)*a1+mh2*vs)*sin/(2*vs**2) 
g_2AA=(np.sqrt(2)*a1+ms2*vs)*cos/(2*vs**2)

fu=0.020
fd=0.026
fs=0.118
fT=1-fu-fd-fs
y=3.9*10**(-28)*mp**4/(2*np.pi*v**2*(mp+mA)**2)*(g_1AA*cos/mh2-g_2AA*sin/ms2)**2*(fu+fd+fs+fT*2/9)**2
y=data[:,8]*10**(-36)
y=y*omega/0.1196
"""
index1 = np.where((order==1) & (omega<0.11))
index2 = np.where((order==1) & (omega>=0.11))
index3 = np.where((order!=1) & (omega<0.11))
index4 = np.where((order!=1) & (omega>=0.11))
"""
index1 = np.where((order==1) & (strength>1) & (omega<0.11))
index2 = np.where((order==1) & (strength>1) & (omega>=0.11))
index3 = np.where(((order!=1) | ((order==1) & (strength<=1))) & (omega<0.11))
index4 = np.where(((order!=1) | ((order==1) & (strength<=1))) & (omega>=0.11))

fig1,ax1=plt.subplots(figsize=(8,6), dpi=300)
ax1.scatter(ms[index3],y[index3],s=0.6,c='orange')
ax1.scatter(ms[index4],y[index4],s=0.6,c='coral')
ax1.scatter(ms[index1],y[index1],s=0.6,c='royalblue')
ax1.scatter(ms[index2],y[index2],s=0.6,c='red')
ax1.set_xlabel('$m_{h_2}~\mathrm{[GeV]}$',size=20)
ax1.set_ylabel('$rescaled~\sigma_{SI}~\mathrm{[cm^2]}$',size=20)
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_xlim(60,1000)
ax1.set_ylim(10e-57, 10e-25)
patch_1 = mpatches.Patch(color='royalblue', label='SFOPT $\Omega$<0.11')
patch_2 = mpatches.Patch(color='red', label='SFOPT $\Omega$>0.11')
patch_3 = mpatches.Patch(color='orange', label='OtherPT $\Omega$<0.11')
patch_4 = mpatches.Patch(color='coral', label='OtherPT $\Omega$>0.11')
l1, = ax1.plot(x_1_,y_1_,color='black',linewidth=2,label='XENON1T')
l2, = ax1.plot(x_n_,y_n_,color='black',linewidth=2,linestyle='--',label='XENONnT')
ax1.legend(handles=[patch_1,patch_3,patch_4,l1,l2])
fig1.savefig("sigma_mS_2body.png")
"""
fig2,ax2=plt.subplots(figsize=(8,6), dpi=300)
ax2.scatter(mA[index3],y[index3],s=0.6,c='orange')
ax2.scatter(mA[index4],y[index4],s=0.6,c='coral')
ax2.scatter(mA[index1],y[index1],s=0.6,c='royalblue')
ax2.scatter(mA[index2],y[index2],s=0.6,c='red')
ax2.set_xlabel('$\mathrm{M_A (GeV)}$')
ax2.set_ylabel('$\mathrm{rescaled \sigma_{SI} (cm^2)}$')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.set_ylim(10e-57, 10e-29)
ax2.set_xlim(60, 0.5*(1000-120))
patch_1 = mpatches.Patch(color='royalblue', label='First_order Omega<0.11')
patch_2 = mpatches.Patch(color='red', label='First_order Omega>0.11')
patch_3 = mpatches.Patch(color='orange', label='Second_order Omega<0.11')
patch_4 = mpatches.Patch(color='coral', label='Second_order Omega>0.11')
l1, = ax2.plot(x_1,y_1,color='black',linewidth=2,label='XENON1T')
l2, = ax2.plot(x_n,y_n,color='black',linewidth=2,linestyle='--',label='XENONnT')
ax2.legend(handles=[patch_1,patch_2,patch_3,patch_4,l1,l2])
fig2.savefig("sigma_mA_2body.png")
"""