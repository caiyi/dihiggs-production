import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import scan.cxSM as cxSM
from higgs_decay_width import *
import fileinput

#input_file=['scan/first_order_2body.dat','scan/first_order_3body.dat']
#input_file=['scan/others_2body/others_0.dat']
input_file=[]
for n in range(1000):
    input_file.append(os.path.join('try_output',str(n),'result.txt'))
for n in range(500):
    input_file.append(os.path.join('try1_output',str(n),'result.txt'))
for n in range(1000):
    input_file.append(os.path.join('try2_output',str(n),'result.txt'))
data=np.loadtxt(fileinput.input(input_file))

x2=[300,400,500]
y2=[0.010532,0.00510299,0.00229842]
x3=[300,700,1000]
y3=[0.002354,0.00100718,0.000899193]

omega=data[:,14]
strength=data[:,7]
mp=938.2720813*0.001
v0=246.0*np.ones(len(data))
a1=data[:,0]
x0=data[:,1]
s1=data[:,2]
c1=np.sqrt(1-s1**2)
mh=data[:,3]
ms=data[:,4]
mA=data[:,5]
order=data[:,6]
mh2=mh**2
ms2=ms**2
mA2=mA**2
delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)

ary_a1=np.array(a1); ary_x0=np.array(x0); ary_v0=np.array(v0); ary_s1=np.array(s1)
ary_mh2=np.array(mh2); ary_ms2=np.array(ms2); ary_mA2=np.array(mA2)

cxSM_decay_class = cxSM_decay_width(ary_a1, ary_x0, ary_v0, ary_s1, ary_mh2, ary_ms2, ary_mA2)
cro_sec_h2_1 = cxSM_decay_class.cro_sec_2body_tmp()
cro_sec_h2_2 = cxSM_decay_class.cro_sec_2body()
cro_sec_h2_3 = cxSM_decay_class.cro_sec_3body()
br_h_hAA = cxSM_decay_class.BR_h2_h1AA()

idx1 = np.where((cro_sec_h2_3>0.0) & (cro_sec_h2_2==0.0))
idx2 = np.where(cro_sec_h2_2>0.0)

fig=plt.figure(figsize=(22,6), dpi=150)
fig2 = fig.add_subplot(131)
fig3 = fig.add_subplot(132)
fig4 = fig.add_subplot(133)

Cro_Sec_2 = fig2.scatter(ms[idx2],cro_sec_h2_1[idx2],s=0.6,c=strength[idx2])
fig2.set_title("2-body without "+r'$\mathrm{h_1 \to AA}$',size=20)
fig2.set_yscale('log')
fig2.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(a)",size=20)
fig2.set_ylabel(r'$\sigma~\mathrm{[pb]}$',size=20)
fig.colorbar(Cro_Sec_2,ax=fig2)

Cro_Sec_3 = fig3.scatter(ms[idx1],cro_sec_h2_3[idx1],s=0.6,c=strength[idx1])
fig3.set_title("3-body",size=20)
fig3.set_yscale('log')
fig3.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(b)",size=20)
fig3.set_ylabel(r'$\sigma~\mathrm{[pb]}$',size=20)
fig.colorbar(Cro_Sec_3,ax=fig3)

Cro_Sec_4 = fig4.scatter(ms[idx1],br_h_hAA[idx1],s=0.6,c=strength[idx1])
fig4.set_title("3-body",size=20)
fig4.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(c)",size=20)
fig4.set_ylabel(r'$BR(h_{2}\to h_{1}AA)$',size=20)
fig.colorbar(Cro_Sec_4,ax=fig4)

fig2.set_xlim([300,1000])
fig3.set_xlim([300,1000])
fig4.set_xlim([300,1000])

fig.savefig("cross_tmp.png",bbox_inches="tight")