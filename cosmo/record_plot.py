import matplotlib.pyplot as plt
import numpy as np
import math
import sys
from scipy import interpolate
import scan.cxSM as cxSM
from higgs_decay_width_new import *
import fileinput
import os

data=np.loadtxt("record.txt")
x2=[300,400,500]
y2=[0.010532,0.00510299,0.00229842]
x3=[300,700,1000]
y3=[0.002354,0.00100718,0.000685853]

mode=data[:,0]
ms=data[:,1]
xsec=data[:,2]
strength=data[:,3]

idx4 = np.where(mode==2)
idx5 = np.where(mode==3)

fig=plt.figure(figsize=(15,6), dpi=150)
fig1 = fig.add_subplot(121)
fig2 = fig.add_subplot(122)

Cro_Sec_4 = fig1.scatter(ms[idx4],xsec[idx4]/19*30,s=20,c=strength[idx4],cmap='Blues')
Cro_Sec_5 = fig2.scatter(ms[idx5],xsec[idx5],s=20,c=strength[idx5],cmap='Oranges')
fig1.set_yscale('log')
fig1.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig1.set_ylabel(r'$\sigma_{2~body}~\mathrm{[pb]}$',size=20)
fig2.set_yscale('log')
fig2.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig2.set_ylabel(r'$\sigma_{3~body}~\mathrm{[pb]}$',size=20)
cb1=fig.colorbar(Cro_Sec_4,ax=fig1)
cb2=fig.colorbar(Cro_Sec_5,ax=fig2)
l1,=fig1.plot([300,400,500],[0.0107995,0.00497448,0.0021281],color='black',linewidth=2,label='95% CLs')
fig1.plot([500,700,1000],[0.00217955,0.000796386,0.000685853],color='black',linewidth=2)
l2,=fig1.plot([300,400,500],[0.0107995/1.96*5,0.00497448/1.96*5,0.0021281/1.96*5],color='gray',linewidth=2,label='5 Sigma')
fig1.plot([500,700,1000],[0.00217955/1.96*5,0.000796386/1.96*5,0.000685853/1.96*5],color='gray',linewidth=2)
fig1.legend(handles=[l2,l1],fontsize=20,loc=3)
l1,=fig2.plot([300,400,500],[0.0107995,0.00497448,0.0021281],color='black',linewidth=2,label='95% CLs')
fig2.plot([500,700,1000],[0.00217955,0.000796386,0.000685853],color='black',linewidth=2)
l2,=fig2.plot([300,400,500],[0.0107995/1.96*5,0.00497448/1.96*5,0.0021281/1.96*5],color='gray',linewidth=2,label='5 Sigma')
fig2.plot([500,700,1000],[0.00217955/1.96*5,0.000796386/1.96*5,0.000685853/1.96*5],color='gray',linewidth=2)
fig2.legend(handles=[l2,l1],fontsize=20,loc=3)
fig1.set_xlim([300,1000])
fig2.set_xlim([300,1000])
fig.savefig("cross_body_new.png")
"""
data1=np.loadtxt("record_pre.txt")

mode=data1[:,0]
ms=data1[:,1]
xsec=data1[:,2]
strength=data1[:,3]

idx6 = np.where(mode==1)
idx7 = np.where(mode==2)
idx8 = np.where(mode==3)

fig=plt.figure(figsize=(22,6), dpi=150)
fig2 = fig.add_subplot(131)
fig3 = fig.add_subplot(132)
fig4 = fig.add_subplot(133)

Cro_Sec_2 = fig2.scatter(ms[idx6],xsec[idx6],s=0.6,c=strength[idx6])
fig2.set_title("2-body without "+r'$\mathrm{h_1 \to AA}$',size=20)
fig2.set_yscale('log')
fig2.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(a)",size=20)
fig2.set_ylabel(r'$\sigma~\mathrm{[pb]}$',size=20)
fig.colorbar(Cro_Sec_2,ax=fig2)

Cro_Sec_3 = fig3.scatter(ms[idx7],xsec[idx7],s=0.6,c=strength[idx7])
fig3.set_title("3-body",size=20)
fig3.set_yscale('log')
fig3.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(b)",size=20)
fig3.set_ylabel(r'$\sigma~\mathrm{[pb]}$',size=20)
fig.colorbar(Cro_Sec_3,ax=fig3)

Cro_Sec_4 = fig4.scatter(ms[idx8],xsec[idx8],s=0.6,c=strength[idx8])
fig4.set_title("3-body",size=20)
fig4.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$'+"\n(c)",size=20)
fig4.set_ylabel(r'$BR(h_{2}\to h_{1}AA)$',size=20)
fig.colorbar(Cro_Sec_4,ax=fig4)

fig2.set_xlim([300,1000])
fig3.set_xlim([300,1000])
fig4.set_xlim([300,1000])

fig.savefig("cross_new_try4.png",bbox_inches="tight")
"""