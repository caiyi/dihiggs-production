import numpy as np
import random
import cxSM as cxSM
import sys
import os
import time
import eventlet
eventlet.monkey_patch()

f=open("chenning/first.dat", 'a+')
for i in range(0,10000):
    mh=125.0
    mh2=mh**2
    ms=random.uniform(65.0,150.0)
    ms2=ms**2
    mA2=random.uniform(65, 2000.0)**2
    x0=random.uniform(0.0, 150.0)
    theta=random.uniform(0,0.5)
    s1=np.sin(theta)
    a1=random.uniform(-100.0**3, 100.0**3)

    c1=np.sqrt(1-s1**2)
    v0=246.0
    lam=(2*(mh2*c1**2+ms2*s1**2))/v0**2
    delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
    d2=2*(np.sqrt(2)*a1+ms2*x0*c1**2+mh2*x0*s1**2)/(x0**3)
    b1=(-np.sqrt(2)*a1-mA2*x0)/x0
    mu2=1.0/2.0*(-x0**2*delta2-v0**2*lam)
    sum12=(-4*np.sqrt(2)*a1-d2*x0**3-v0**2*x0*delta2)/(2*x0)
    b2=sum12-b1

    try:
        model = cxSM.ComplexSingletSM(x0, lam, mu2, delta2, a1, b1, b2, d2)
        phases = model.getPhases()
        tctrans = model.calcTcTrans()
    except:
        continue
    else:
        ms=np.sqrt(ms2)
        mh=np.sqrt(mh2)
        mA=np.sqrt(mA2)
        vs=x0
        EW_vac_phase = None
        phase_diff = np.inf
        for key, phase in phases.items():
            vev = phase.valAt(T=0.0)
            if abs(vev[0] - v0) < phase_diff:
                phase_diff = abs(vev[0] - v0)
                EW_vac_phase = key

        for trans in tctrans:
            if trans['low_phase'] == EW_vac_phase and trans['trantype'] == 1:
                try:
                    alltran = model.findAllTransitions()
                except:
                    continue
                else:
                    alltrans = [x for x in alltran if x is not None]
                    if len(alltrans) > 0:
                        trans_dict = {}
                        for trans_count, trans in enumerate(alltrans):
                            trans_dict = {'Critical Temperature': trans['crit_trans']['Tcrit'],
                            'Nucleation Temperature': trans['Tnuc'],
                            'Action': trans['action'],
                            'criT Lvev1': trans['crit_trans']['low_vev'][0],
                            'criT Lvev2': trans['crit_trans']['low_vev'][1],
                            'criT Hvev1': trans['crit_trans']['high_vev'][0],
                            'criT Hvev2': trans['crit_trans']['high_vev'][1],
                            'Trtype': trans['trantype'],
                            'phi1 High VEV': trans['high_vev'][0],
                            'phi1 Low VEV': trans['low_vev'][0],
                            'phi2 High VEV': trans['high_vev'][1],
                            'phi2 Low VEV': trans['low_vev'][1]}
                            phic_Tc=np.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2+(trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']
                            Hc_Tc=np.sqrt((trans_dict['criT Hvev1']-trans_dict['criT Lvev1'])**2)/trans_dict['Critical Temperature']
                            sc_Tc=np.sqrt((trans_dict['criT Hvev2']-trans_dict['criT Lvev2'])**2)/trans_dict['Critical Temperature']
                            if trans['trantype'] == 1:
                                f.write(str(a1)+'  '+str(vs)+'  '+str(s1)+'  '+str(mh)+'  '+str(ms)+'  '+str(mA)+'  '+str(trans['trantype'])+'  '+str(phic_Tc)+'  '+str(Hc_Tc)+'  '+str(sc_Tc)+'\n')
                                f.flush()
                        
f.close()
