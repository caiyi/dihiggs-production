import numpy as np
import cxSM as cxSM
from higgs_decay_width import *


f=open('./test_cxSM.dat', 'a+')

mh2=random.uniform(120.0**2, 130.0**2); ms2=random.uniform(65.0**2,500.0**2); mA2=random.uniform(10.0**2, 2000.0**2); x0=random.uniform(0.01, 150.0); s1=random.uniform(0.01,0.5); a1=random.uniform(-100.0**3, 100.0**3)

c1=np.sqrt(1-s1**2)
v0=246.0
lam=(2*(mh2*c1**2+ms2*s1**2))/v0**2
delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
d2=2*(np.sqrt(2)*a1+ms2*x0*c1**2+mh2*x0*s1**2)/(x0**3)
b1=(-np.sqrt(2)*a1-mA2*x0)/x0
mu2=1.0/2.0*(-x0**2*delta2-v0**2*lam)
sum12=(-4*np.sqrt(2)*a1-d2*x0**3-v0**2*x0*delta2)/(2*x0)
b2=sum12-b1

model = cxSM.ComplexSingletSM(x0, lam, mu2, delta2, a1, b1, b2, d2)
model.findAllTransitions()
tctrans = model.findAllTransitions()
for trans in tctrans:
    print(trans)
    if trans['trantype'] == 1:
        f.write(str(a1)+'  '+str(x0)+'  '+str(s1)+'  '+str(np.sqrt(mh2))+'  '+str(np.sqrt(ms2))+'  '+str(np.sqrt(mA2))+'\n')
        f.flush()

f.close()