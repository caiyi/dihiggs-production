import subprocess
import numpy as np
import sys
import os
import fileinput

n=int(sys.argv[1])
filelist = []
for name in os.listdir("/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/try3"):
    filelist.append(os.path.join("/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/try3",name))
for iii in range(60):
    input_file.append(filelist[60*n+iii])
data=np.loadtxt(fileinput.input(input_file))

output=open("result.txt", mode='a+')
#output.write("a1  x0  SIN  MH  Mh2  MAh  trantype  protonSI_1  neutronSI_1  protonSI_2  neutronSI_2  Omega_1h^2  Omega_2h^2  Omega")

for i in range(data.shape[0]):
    a1=data[i,0]
    x0=data[i,1]
    SIN=data[i,2]
    MH=data[i,3]
    Mh2=data[i,4]
    MAh=data[i,5]
    trantype=int(data[i,6])
    strength=data[i,8]/np.sqrt(2)

    input=open("input.par", mode='w')
    input.write("""a1  {}
x0  {}
SIN {}
MH  {}
Mh2 {}
MAh {}""".format(a1,x0,SIN,MH,Mh2,MAh))
    input.close()

    output.write("\n{}  {}  {}  {}  {}  {}  {}  {}  ".format(a1,x0,SIN,MH,Mh2,MAh,trantype,strength))
    output.flush()

    subprocess.run("/besfs5/users/cyz/dihiggs-production/feyn/micromegas_5.2.7.a/cxSM/main input.par", shell=True)

output.close()