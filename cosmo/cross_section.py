import numpy as np
from higgs_decay_width import *
import matplotlib.pyplot as plt

vs=50
a1_1=-500000
a1_2=-550000
a1_3=-600000

mA = np.arange(40, 62.5, 0.1)
v=246.0*np.ones(len(mA))
x0=vs*np.ones(len(mA))
sin=-0.4*np.ones(len(mA))
mh2=125.**2*np.ones(len(mA))
ms2=300.**2*np.ones(len(mA))
mA2=mA**2

a_1=a1_1*np.ones(len(mA))
a_2=a1_2*np.ones(len(mA))
a_3=a1_3*np.ones(len(mA))

cxSM_decay_class_1 = cxSM_decay_width(a_1,x0,v,sin,mh2,ms2,mA2)
BR_h1_AA_1 = cxSM_decay_class_1.BR_h1_AA()
cxSM_decay_class_2 = cxSM_decay_width(a_2,x0,v,sin,mh2,ms2,mA2)
BR_h1_AA_2 = cxSM_decay_class_2.BR_h1_AA()
cxSM_decay_class_3 = cxSM_decay_width(a_3,x0,v,sin,mh2,ms2,mA2)
BR_h1_AA_3 = cxSM_decay_class_3.BR_h1_AA()

plt.text(40,0.92,"sin=-0.4,mh1=125GeV,vs={}GeV".format(vs),fontsize=13)
plt.text(64,0.22,'22%',color="red")
plt.text(64,0.019,'1.9%',color="black")
plt.plot(mA, BR_h1_AA_1, label="a1={}".format(a1_1))
plt.plot(mA, BR_h1_AA_2, label="a1={}".format(a1_2))
plt.plot(mA, BR_h1_AA_3, label="a1={}".format(a1_3))
plt.axhline(y=0.22, color='red', linestyle='--')
plt.axhline(y=0.019, color='black', linestyle='--')
plt.ylim(0, 1)
plt.xlabel("mA {GeV}")
plt.ylabel("BR(h1->AA)")
plt.legend()
plt.savefig("{}.png".format(vs))