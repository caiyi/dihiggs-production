#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

aplt.set_atlas_style()
fig, ax = aplt.subplots(name="fig1", figsize=(800, 600))
root.gSystem.Load("libDelphes.so")
cudir = os.path.dirname(os.path.abspath(__file__))

hist=[]
dict = {'d_s':[0.02],'d_v':[40],'d_a':[1000000],'d_mh':[80],'d_ma':[30]}
name = {'d_s':'sin#theta','d_v':'vev_{s}','d_a':'a_{1}','d_mh':'m_{h2}','d_ma':'m_{ah}'}
color = [root.kRed,root.kOrange,root.kBlue,root.kAzure,root.kGreen,root.kYellow,root.kGray,root.kBlack]
re = None
#dict['d_s']=[0.01,0.02,0.03,0.05,0.1,0.5]
#dict['d_v']=[10,30,40,50,70]
#dict['d_a']=[-1000000,0,1000000]
#dict['d_mh']=[30,60,80,100,130]
dict['d_ma']=[10,20,30,40,60,80]

for i in dict:
    if (len(dict[i])!=1):
        re = i

for s in dict['d_s']:
    for v in dict['d_v']:
        for a in dict['d_a']:
            for mh in dict['d_mh']:
                for ma in dict['d_ma']:

                    dir = "MG5_{}_{}_{}_{}_{}".format(s,v,a,mh,ma)
                    filename = os.path.join("/scratchfs/bes/cyz",dir,"Events/run_01/tag_1_delphes_events.root")

                    chain = root.TChain("Delphes")
                    chain.Add(filename)
                    treeReader = root.ExRootTreeReader(chain)
                    n = treeReader.GetEntries()
                    b_MET = treeReader.UseBranch("MissingET")
                    n_h = len(hist)

                    hist.append(root.TH1D("{}".format(n_h),'',50,0,500))
                    for i in range(0,n):
                        treeReader.ReadEntry(i)
                        met = b_MET.At(0)
                        hist[-1].Fill(met.MET)
                    ax.plot(hist[-1],linecolor=color[n_h]+1,label="{} = {}".format(name[re],dict[re][n_h]),labelfmt="L")

ax.add_margins(top=0.1)
ax.set_xlabel("MissingET [GeV]")
ax.set_ylabel("Events / 10GeV")
ax.text(0.2, 0.93, "sin#theta=0.02,vev_{s}=40,a_{1}=1e6", size=22, align=13)
ax.text(0.3, 0.88, "m_{h2}=80,m_{ah}=30", size=22, align=13)
ax.legend(loc=(0.65, 0.68, 0.80, 0.90))
fig.savefig(os.path.join(cudir,"cali","hist_{}.png".format(name[re])))