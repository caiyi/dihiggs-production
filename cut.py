#!/usr/bin/env python3
import os
import ROOT as root
import numpy as np
import atlasplots as aplt

aplt.set_atlas_style()
fig1, ax1 = aplt.subplots(name="fig1", figsize=(800, 600))
root.gSystem.Load("libDelphes.so")
cudir = os.path.dirname(os.path.abspath(__file__))

hist_met=[]
name = {'d_s':'sin#theta','d_v':'vev_{s}','d_a':'a_{1}','d_mh':'m_{h2}','d_ma':'m_{ah}'}
color = [root.kRed,root.kOrange,root.kBlue,root.kAzure,root.kGreen,root.kYellow,root.kGray,root.kBlack]
dict = {
    'Background':["/scratchfs/bes/cyz/MG5_0.02_40_1000000_300_60_b/Events/run_01/tag_1_delphes_events.root"],
    'Signal':["/scratchfs/bes/cyz/MG5_0.02_40_1000000_300_60_s/Events/run_01/tag_1_delphes_events.root"],
}

for f in dict.keys():
    chain = root.TChain("Delphes")
    chain.Add(dict[f][0])
    treeReader = root.ExRootTreeReader(chain)
    n = treeReader.GetEntries()
    b_MET = treeReader.UseBranch("GenMissingET")
    n_h = len(hist_met)

    hist_met.append(root.TH1D("{}".format(n_h),'',50,0,200))
    for i in range(0,n):
        treeReader.ReadEntry(i)
        met = b_MET.At(0)
        hist_met[-1].Fill(met.MET)
    ax1.plot(hist_met[-1],linecolor=color[n_h]+1,label=f,labelfmt="L")

ax1.add_margins(top=0.1)
ax1.set_xlabel("MissingET [GeV]")
ax1.set_ylabel("Events / 4GeV")
ax1.legend(loc=(0.65, 0.68, 0.80, 0.90))
fig1.savefig(os.path.join(cudir,"cut","MET.png"))