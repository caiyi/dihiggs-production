import matplotlib.pyplot as plt
import numpy as np
import math
import sys
from higgs_decay_width_new import *
import os
import fileinput
input_file=[]
#input_file=['/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/first_order_2body.dat','/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/scan/first_order_3body.dat']
for n in range(1000):
    input_file.append(os.path.join('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/try_output',str(n),'result.txt'))
#for n in range(500):
#    input_file.append(os.path.join('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/try1_output',str(n),'result.txt'))
#for n in range(1000):
#    input_file.append(os.path.join('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/try2_output',str(n),'result.txt'))
data=np.loadtxt(fileinput.input(input_file))


def kth_root(x,k):
    if k % 2 != 0:
        res = np.power(np.abs(x),1./k)
        return res*np.sign(x)
    else:
        return np.power(np.abs(x),1./k)

'''
strength=data[:,7]
ms=data[:,4]; a1=data[:,0]; v0=246.0; s1=data[:,2];
ms=data[:,4]; mA=data[:,5]; ms2=ms**2; mh=data[:,3];mh2=mh**2; mA2=mA**2;
x0=data[:,1]; c1=np.sqrt(1-s1**2); delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0);
'''
s1=data[:,2]; mh=data[:,3]; a1=data[:,0]; v0=246.0
ms=data[:,4]; mA=data[:,5]; ms2=ms**2; mh2=mh**2; mA2=mA**2
x0=data[:,1]; c1=np.sqrt(1-s1**2); delta2=(2*(mh2-ms2)*c1*s1)/(x0*v0)
strength=data[:,8]/np.sqrt(2)

ary_a1=np.array(a1); ary_x0=np.array(x0); ary_v0=np.array(v0); ary_s1=np.array(s1)
ary_mh2=np.array(mh2); ary_ms2=np.array(ms2); ary_mA2=np.array(mA2)

cxSM_decay_class = cxSM_decay_width(ary_a1, ary_x0, ary_v0, ary_s1, ary_mh2, ary_ms2, ary_mA2)

'''
ary_a1=np.array(a1); ary_x0=np.array(x0); ary_v0=np.array(v0); ary_s1=np.array(s1);
ary_mh2=mh2*np.ones(ary_x0.shape); ary_ms2=np.array(ms2); ary_mA2=np.array(mA2);
print('ary_mh2={}'.format(ary_mh2)); print('ary_ms2={}'.format(ary_ms2)) 
cxSM_decay_class = cxSM_decay_width(ary_a1, ary_x0, ary_v0, ary_s1, ary_mh2, ary_ms2, ary_mA2)
'''
g_111, g_211, g_1AA, g_2AA = cxSM_decay_class.cxSM_coupling()




gam_h1tt = cxSM_decay_class.h2_h1tt_decay_width(); gam_h1ff = cxSM_decay_class.h2_h1ff_decay_width(); gam_h1AA = cxSM_decay_class.h2_h1AA_decay_width(); 

h2_2body_dw = cxSM_decay_class.h2_2body_decay_dw();



h2_dw = gam_h1tt + gam_h1AA + h2_2body_dw;
br_h1AA = gam_h1AA/h2_dw
br_h1h1 = cxSM_decay_class.h2_h1h1_decay_width()/h2_dw 

br_tau_tau = cxSM_decay_class.h2_tau_dw()/h2_dw
br_WW = cxSM_decay_class.h2_WW_dw()/h2_dw
br_ZZ = cxSM_decay_class.h2_ZZ_dw()/h2_dw
br_bb = cxSM_decay_class.h2_bb_dw()/h2_dw
br_yy = cxSM_decay_class.h2_yy_dw()/h2_dw



'''
cro_sec = cxSM_decay_class.h2_produce_crosec()
idx = np.where((gam_h1AA>0.0) & (np.absolute(theta)<30.0))
cro_sec_h1AA = cro_sec*br_h1AA
cro_sec[idx] = np.log10(cro_sec_h1AA[idx])
'''

br_h_bb = 0.577; 
br_w_ev = 0.1046; br_w_uv = 0.1046; br_w_qq = 0.6832; br_w_lv = 3.0*0.1046;


cx_bbH_bb = (cxSM_decay_class.h2_cx_all()[2]*br_bb)
cx_bbH_Tau = (cxSM_decay_class.h2_cx_all()[2]*br_tau_tau)

cx_ggf_Tau = (cxSM_decay_class.h2_cx_all()[0]*br_tau_tau)
cx_ggf_WWZZ = (cxSM_decay_class.h2_cx_all()[0]*(br_WW + br_ZZ))

cx_VBF_WWZZ = (cxSM_decay_class.h2_cx_all()[1]*(br_WW + br_ZZ))


idx = np.where((br_h1h1>0.0))
cx_ggF_VBF_HH = np.zeros(br_h1h1.shape)
cx_ggF_VBF_HH[idx] = ((cxSM_decay_class.h2_cx_all()[1][idx] + cxSM_decay_class.h2_cx_all()[0][idx])*br_h1h1[idx])


fig = plt.figure(figsize=(8,6), dpi=150)
sec_fig = plt.figure(figsize=(15,6), dpi=150)
thd_fig = plt.figure(figsize=(15,6), dpi=150)
four_fig = plt.figure(figsize=(8,6), dpi=150)

fig1 = fig.add_subplot(111); 
fig2 = sec_fig.add_subplot(121); fig3 = sec_fig.add_subplot(122);
fig4 = thd_fig.add_subplot(121); fig5 = thd_fig.add_subplot(122);
fig6 = four_fig.add_subplot(111);

VBF_WWZZ = fig1.scatter(ms[:],cx_VBF_WWZZ[:],s=0.6,c=abs(s1[:]))
df_1 = np.loadtxt("vbf_VV.txt")
x_1=df_1[:,0]*1000
y_1=df_1[:,1]*4260/3766
fig1.plot(x_1,y_1,color='black',linewidth=2,label='arXiv: 2004.14636')

ggf_Tau = fig2.scatter(ms[:],cx_ggf_Tau[:],s=0.6,c=abs(s1[:]))
df_2 = np.loadtxt("ggf_tau.txt")
x_2=df_2[:,0]
y_2=df_2[:,1]*54.72/48.61
fig2.plot(x_2,y_2,color='black',linewidth=2,label='arXiv: 2002.12223')
ggf_WWZZ = fig3.scatter(ms[:],cx_ggf_WWZZ[:],s=0.6,c=abs(s1[:]))
df_3 = np.loadtxt("ggf_VV.txt")
x_3=df_3[:,0]*1000
y_3=df_3[:,1]*54.72/48.61
fig3.plot(x_3,y_3,color='black',linewidth=2,label='arXiv: 2004.14636')
ggf_h1h1 = fig6.scatter(ms[idx],cx_ggF_VBF_HH[idx],s=0.6,c=abs(s1[idx]))
df_a = np.loadtxt("bbbb.txt")
x_a=df_a[:,0]
y_a=df_a[:,1]*54.72/48.61
fig6.plot(x_a,y_a,color='red',linewidth=2,label=r'$b \bar{b} b \bar{b}~139fb^{-1}$, ATLAS-CONF-2021-035')
df_b = np.loadtxt("bbtt.txt")
x_b=df_b[:,0]
y_b=df_b[:,1]*54.72/48.61
fig6.plot(x_b,y_b,color='blue',linewidth=2,label=r'$b \bar{b} \tau^+ \tau^-~139fb^{-1}$, ATLAS-CONF-2021-030')
df_c = np.loadtxt("bbyy.txt")
x_c=df_c[:,0]
y_c=df_c[:,1]*54.72/48.61
fig6.plot(x_c,y_c,color='green',linewidth=2,label=r'$b \bar{b} \gamma \gamma~139fb^{-1}$, ATLAS-CONF-2021-016')
df_d = np.loadtxt("comb.txt")
x_d=df_d[:,0]
y_d=df_d[:,1]*54.72/48.61
fig6.plot(x_d,y_d,color='black',linewidth=2,label=r'Combined $27.5-36.1fb^{-1}$, arXiv: 1906.02025')
'''
df_6 = np.loadtxt("ggf_hh.txt")
x_6=df_6[:,0]
y_6=df_6[:,1]*54.72/48.61/1000
fig6.plot(x_6,y_6,color='black',linewidth=2,label='arXiv: 2112.11876')
'''

bbH_bb = fig4.scatter(ms[:],cx_bbH_bb[:],s=0.6,c=abs(s1[:]))
df_4 = np.loadtxt("bbh_bb.txt")
x_4=df_4[:,0]
y_4=df_4[:,1]*5.529/4.88
fig4.plot(x_4,y_4,color='black',linewidth=2,label='arXiv: 1907.02749')
bbH_Tau = fig5.scatter(ms[:],cx_bbH_Tau[:],s=0.6,c=abs(s1[:]))
df_5 = np.loadtxt("bbh_tau.txt")
x_5=df_5[:,0]
y_5=df_5[:,1]*5.529/4.88
fig5.plot(x_5,y_5,color='black',linewidth=2,label='arXiv: 2002.12223')

fig1.set_xlim([300,1000])
fig2.set_xlim([300,1000])
fig3.set_xlim([300,1000])
fig4.set_xlim([300,1000])
fig5.set_xlim([300,1000])
fig6.set_xlim([300,1000])

fig1.legend(fontsize=20)
fig2.legend(fontsize=20)
fig3.legend(fontsize=20)
fig4.legend(fontsize=20)
fig5.legend(fontsize=20)
fig6.legend()

fig2.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig2.set_ylabel(r'$\sigma^{ggF}_{h_2 \to \tau \tau}~\mathrm{[pb]}$',size=20)
fig3.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig3.set_ylabel(r'$\sigma^{ggF}_{h_2 \to WW+ZZ}~\mathrm{[pb]}$',size=20)
fig6.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig6.set_ylabel(r'$\sigma^{ggF}_{h_2 \to h_1 h_1}~\mathrm{[pb]}$',size=20)

fig1.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig1.set_ylabel(r'$\sigma^{VBF}_{h_2 \to WW+ZZ}~\mathrm{[pb]}$',size=20)

fig4.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig4.set_ylabel(r'$\sigma^{bbH}_{h_2 \to bb}~\mathrm{[pb]}$',size=20)
fig5.set_xlabel(r'$m_{h_2}~\mathrm{[GeV]}$',size=20)
fig5.set_ylabel(r'$\sigma^{bbH}_{h_2 \to \tau \tau}~\mathrm{[pb]}$',size=20)


fig.colorbar(VBF_WWZZ, ax=fig1)

sec_fig.colorbar(ggf_WWZZ, ax=fig3)
sec_fig.colorbar(ggf_Tau, ax=fig2)

thd_fig.colorbar(bbH_bb, ax=fig4)
thd_fig.colorbar(bbH_Tau, ax=fig5)

four_fig.colorbar(ggf_h1h1, ax=fig6)

fig1.set_yscale("log")
fig2.set_yscale("log")
fig3.set_yscale("log")
fig4.set_yscale("log")
fig5.set_yscale("log")
fig6.set_yscale("log")

fig.savefig("tryATLAS_res_VBF_new.png")
sec_fig.savefig("tryATLAS_res_ggf_new.png")
thd_fig.savefig("tryATLAS_res_bbH_new.png")
four_fig.savefig("tryATLAS_res_diHiggs_new.png")
