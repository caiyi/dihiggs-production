import numpy as np
import math
import pickle
import random
import sys
import pandas as pd
from scipy import interpolate
from matplotlib.pylab import plt
from scipy import linalg as la
from scipy import integrate


df_br=pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/BR_new.csv', sep=',',header=None)
df_xsec14 = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/cosmo/XS14TEV.txt', delimiter="\t", header=None)
#interpolated ggH cross-section in pb at 14TeV 
Xsec14_ggH = interpolate.interp1d(df_xsec14.iloc[:,0].values,df_xsec14.iloc[:,1].values)

MH_list = df_br.iloc[:, 0].values
total_Width_list = df_br.iloc[:,-1].values





ggf_xsec14 = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/ggF_Higgs.txt', delimiter="\t", header=None)

VBF_xsec14 = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/VBF_Higgs.txt', delimiter="\t", header=None)
     
bbH_xsec14 = pd.read_csv('/afs/ihep.ac.cn/users/c/cyz/besfs/dihiggs-production/h2decay/bbH_Higgs.txt', delimiter="\t", header=None)
 
Xsec14_ggf = interpolate.interp1d(ggf_xsec14.iloc[:,0].values,ggf_xsec14.iloc[:,1].values)
Xsec14_VBF = interpolate.interp1d(VBF_xsec14.iloc[:,0].values,VBF_xsec14.iloc[:,1].values)
Xsec14_bbH = interpolate.interp1d(bbH_xsec14.iloc[:,0].values,bbH_xsec14.iloc[:,1].values)





#created interpolated function for the decay width of SM_like Higgs of different mass
channel_list = ["bb","tautau","mumu","cc","tt","gg","yy","Zy","WW","ZZ"]
interWidth = {}
iter = 1
for channel in channel_list:
    tempBr_list = df_br.iloc[:,iter].values
    tempWidth_list = total_Width_list*tempBr_list
    interWidth[channel] = interpolate.interp1d(MH_list,tempWidth_list)
    iter =  iter + 1
interWidth["total"] = interpolate.interp1d(MH_list,total_Width_list)

#function return the Branching ratio of the heavy Higgs in xSM:
#usage example: HBR("bb", 500., 0.001, 0.01)
def HBR(channel, mH, sintheta, Width_H2h):
    total_width = interWidth["total"](mH)*sintheta**2 + Width_H2h
    print(interWidth["total"](mH))
    if channel == "hh":
        return Width_H2h/total_width
    else:
        return interWidth[channel](mH)*sintheta**2/total_width


def XS14TEVggH(mH, sintheta):
    return Xsec14_ggH(mH)*sintheta**2
def XS14TEVggf(mH, sintheta):
    return Xsec14_ggf(mH)*sintheta**2
def XS14TEVVBF(mH, sintheta):
    return Xsec14_VBF(mH)*sintheta**2
def XS14TEVbbH(mH, sintheta):
    return Xsec14_bbH(mH)*sintheta**2





##Nc is the color quantum number.
def h_ff(mh, mf, Nc, vev):
    y=Nc/(8*math.pi)*(mf**2/vev**2)*mh*pow((1-4*mf**2/mh**2),3.0/2.0)
    return y


##For W boson, Nc=2. For Z boson, Nc=1.
def h_AA(mh, mA, Nc, vev):
    y=Nc/(8*math.pi)*(pow(mA,4)/(mh*vev**2))*(pow((1-4*mA**2/mh**2),1.0/2.0))*(3+1.0/4.0*pow(mh/mA,4)-mh**2/mA**2)
    return y



###Consider top quark loop
def Gamma_LO(m_ratio):
    y=1+7.0/15.0*m_ratio+1543.0/6000.0*m_ratio**2+226.0/1575.0*pow(m_ratio,3)
    return y


def h_gg(mh, mt):
    alpha_s=1; vev=246.0;
    Gf=math.sqrt(2)/(2*vev**2)
    y=Gf*pow(mh,3)/(36*math.sqrt(2)*math.pi)*pow(alpha_s/math.pi,2)*Gamma_LO(mh**2/(4*mt**2))
    #y=pow(mh,3)/(8*math.pi*vev**2)*pow(alpha_s/math.pi,2)*pow(m/mh,4)*Dn(pow(m/mh,2))
    return y


###Total decay width
def h_SM(mh, vev):
    mb=4.18; mtau=1.777*pow(10,-3); mc=1.275; mW=80.379; mZ=91.1876; mt=173.0;
    h_width=h_ff(mh, mb, 3, vev)+h_ff(mh, mc, 3, vev)+h_ff(mh, mtau, 1, vev)+h_AA(mh, mW, 2, vev)+h_AA(mh, mZ, 1, vev)+h_gg(mh, mt)
    return h_width




###Decay width of h2->h1 h1
def h2_h1h1(g_211, mh2, mh1):
    y=g_211**2*math.sqrt(1-4*mh1**2/mh2**2)/(8*math.pi*mh2)
    return y


###Branch ratio
def Br_h2_h1h1(g_211, mh2, mh1, vev, sin_theta):
    br=h2_h1h1(g_211, mh2, mh1)/(h2_h1h1(g_211, mh2, mh1)+sin_theta**2*h_SM(mh2, vev))
    return br



def Br_h2_ZZ(g_211, mh2, mh1, vev, sin_theta):
    mZ=91.1876;
    br=(sin_theta**2*h_AA(mh2, mZ, 1, vev))/(h2_h1h1(g_211, mh2, mh1)+sin_theta**2*h_SM(mh2, vev))
    return br



def Br_h2_h1h1_bbyy(g_211, mh2, mh1, vev, sin_theta):
    h1_bb=0.577; h1_yy=2.28*pow(10,-3);
    br=Br_h2_h1h1(g_211, mh2, mh1, vev, sin_theta)*2*h1_bb*h1_yy;
    return br



def Br_h2_ZZ_llll(g_211, mh2, mh1, vev, sin_theta):
    Z_ll=3.3658*pow(10,-2);
    br=Br_h2_ZZ(g_211, mh2, mh1, vev, sin_theta)*2*Z_ll*Z_ll
    return br



def eff_pp_h2_ZZ_4l(mh):
    y=0.255198 + 0.000638337*mh - 6.04309*pow(10,-7)*pow(mh,2) + 1.93095*pow(10,-10)*pow(mh,3)
    return y



def eff_bbyy_2btag(mh):
    if mh<400.0:
       y=0.103661 - 0.000480608*mh + 1.477*pow(10,-6)*mh**2 - 1.05908*pow(10,-9)*pow(mh,3)
    else:
       y=-0.284147 + 0.00116173*mh - 1.05085*pow(10,-6)*mh**2 + 3.2726*pow(10,-10)*pow(mh,3)
    return y




def eff_bbyy_1btag(mh):
    if mh<400.0:
       y=0.0372772 - 0.000219921*mh + 1.75063*pow(10,-6)*mh**2 - 2.70001*pow(10,-9)*pow(mh,3)
    else:
       y=-0.131216 + 0.000551814*mh - 4.84208*pow(10,-7)*mh**2 + 1.8661*pow(10,-10)*pow(mh,3)
    return y


    




def Mhiggs(a1,a2,b3,b4,lam,v0,x0):
    rst=[]
    US2 = -((a1*v0**2)/(4*x0)) + x0*(b3 + 2*b4*x0)
    UH2 = 2*lam*v0**2;
    USH = v0*(a1 + 2*a2*x0)/2.0
    t2 = -(2*USH)/(US2 - UH2)
    x = t2
    t1 = 1/x*(-1 + math.sqrt(1 + x**2))
    
    c1 = 1/math.sqrt(t1**2 + 1)
    s1 = t1/math.sqrt(t1**2 + 1)
    try:
        ###ms = math.sqrt( US2*c1**2 + UH2*s1**2 - USH*2*c1*s1 )
        ###mhiggs = math.sqrt( US2*s1**2 + UH2*c1**2 + USH*2*c1*s1 )
        Mass_matrix=np.array([[UH2,USH],[USH,US2]])
        eigval,eigvec=la.eig(Mass_matrix)
        eigval=eigval.real
        mh2=eigval[0]
        ms2=eigval[1]
        mhiggs=math.sqrt(mh2); ms=math.sqrt(ms2);
        ###print('mh2=', mh2, '  ms2=', ms2)
    except: 
        return rst;
    ###ms = math.sqrt(( US2*s1**2 + UH2*c1**2 - USH*2*c1*s1 ))
    ###Mass_matrix=np.array([[UH2,USH],[USH,US2]])
    ###eigval,eigvec=la.eig(Mass_matrix)
    ###eigval=eigval.real
    ###mh2=eigval[0]; ms2=eigval[1];
    mhiggs=math.sqrt(mh2)
    ms=math.sqrt(ms2)
    g_211=0.5*(-6*lam*v0*c1**2*s1 + 2*b3*c1*s1**2 + 6*b4*x0*c1*s1**2 + 0.25*a1*(2*c1**3-4*c1*s1**2) + 0.25*a2*(4*x0*c1**3 + 8*v0*c1**2*s1 -8*x0*c1*s1**2 - 4*v0*s1**3))
    ###print('mhigss=',mhiggs,'  ms=',ms,'  g_211=',g_211)
    rst.append(s1)
    rst.append(g_211)
    rst.append(ms)
    rst.append(mhiggs)
    rst.append(c1)
    return rst



def cro_sec_inter(ms_like_mass):
    x = ms_like_mass
    y = 2.89206 - 0.0050712*x + 1.6335*pow(10,-6)*x**2 - 2.6411*pow(10,-10)*x**3
    y0 = np.power(10.0,y)
    return y0




class cxSM_decay_width(object):

      def __init__(self, a1, vs, v0, sin_theta, m12, m22, mA2):
          self.a1=a1; self.vs=vs; self.v0=v0; self.sin_theta=sin_theta; self.m12=np.array(m12); 
          self.m22=np.array(m22); self.mA2=np.array(mA2)
          self.cos_theta=np.sqrt(1-sin_theta**2)
          self.sin_2theta=2*sin_theta*self.cos_theta
          self.m1=np.sqrt(m12); self.m2=np.sqrt(m22); self.mA=np.sqrt(mA2);

          idx1_m2 = np.where(self.m2<=500.0); idx2_m2 = np.where(self.m2>500.0)
          higgs_like_dw=np.zeros(m12.shape)
          higgs_like_dw[idx1_m2]=interWidth["total"](self.m2[idx1_m2])
          higgs_like_dw[idx2_m2]=interWidth["total"](self.m2[idx2_m2])
          #higgs_like_dw[idx2_m2]=cro_sec_inter(self.m2[idx2_m2])

          self.higgs_like_tau_dw=sin_theta**2 * interWidth["tautau"](self.m2)
          self.higgs_like_WW_dw=sin_theta**2 * interWidth["WW"](self.m2)
          self.higgs_like_ZZ_dw=sin_theta**2 * interWidth["ZZ"](self.m2)
          self.higgs_like_bb_dw=sin_theta**2 * interWidth["bb"](self.m2)
          self.higgs_like_yy_dw=sin_theta**2 * interWidth["yy"](self.m2)

          self.higgs_like_decay_width=higgs_like_dw
          self.higgs_SM_decay_width=interWidth["total"](self.m1);
          ##print('h1 width={}'.format(self.higgs_SM_decay_width))
          ##print('h2 width={}'.format(sin_theta**2*self.higgs_like_decay_width))
          self.h2_produce_cro_sec=XS14TEVggH(self.m2, sin_theta)
          self.h2_ggf_cx=XS14TEVggf(self.m2, sin_theta)
          self.h2_VBF_cx=XS14TEVVBF(self.m2, sin_theta)
          self.h2_bbH_cx=XS14TEVbbH(self.m2, sin_theta)


      def h2_produce_crosec(self):
          return self.h2_produce_cro_sec


      def h2_cx_all(self):
          return self.h2_ggf_cx, self.h2_VBF_cx, self.h2_bbH_cx


      def h2_tau_dw(self):
          return self.higgs_like_tau_dw


      def h2_WW_dw(self):
          return self.higgs_like_WW_dw


      def h2_ZZ_dw(self):
          return self.higgs_like_ZZ_dw


      def h2_bb_dw(self):
          return self.higgs_like_bb_dw


      def h2_yy_dw(self):
          return self.higgs_like_yy_dw


      def cxSM_coupling(self):
          cos_theta=self.cos_theta; a1=self.a1; vs=self.vs; v0=self.v0; sin_theta=self.sin_theta; m12=self.m12; m22=self.m22;
          sin_2theta=self.sin_2theta
          g_111=0.5*(m12*cos_theta**3/v0+(math.sqrt(2)*a1+m12*vs)*sin_theta**3/(vs**2))
          g_211=(3*math.sqrt(2)*a1*v0*sin_theta+(2*m12+m22)*vs*(v0*sin_theta-vs*cos_theta))*sin_2theta/(4*v0*vs**2)
          g_1AA=(math.sqrt(2)*a1+m12*vs)*sin_theta/(2*vs**2)
          g_2AA=(math.sqrt(2)*a1+m22*vs)*cos_theta/(2*vs**2)
 
          return g_111, g_211, g_1AA, g_2AA

      def h2_h1h1_decay_width(self):
          m12, m22, m2, m1 = self.m12, self.m22, self.m2, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m2.shape)
          idx1=np.where(m2 > 2*m1)
          ###print('aaaaaaaidx1={}'.format(idx1))
          Gamma[idx1] = g_211[idx1]**2*np.sqrt(1-4*m12[idx1]/m22[idx1])/(8*math.pi*m2[idx1])
          return Gamma

      def h1_AA_decay_width(self):
          m12, mA2, mA, m1 = self.m12, self.mA2, self.mA, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m1.shape)
          idx1 = np.where(m1 > 2*mA)
          Gamma[idx1] = g_1AA[idx1]**2*np.sqrt(1-4*mA2[idx1]/m12[idx1])/(8*math.pi*m1[idx1])
          return Gamma

      def h2_AA_decay_width(self):
          m22, mA2, mA, m2 = self.m22, self.mA2, self.mA, self.m2
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m2.shape)
          idx1 = np.where(m2 > 2*mA)
          Gamma[idx1] = g_2AA[idx1]**2*np.sqrt(1-4*mA2[idx1]/m22[idx1])/(8*math.pi*m2[idx1])
          return Gamma

      def lamd_1_2(self, m12, m1, m2):
          m12_2=m12**2; m1_2=m1**2; m2_2=m2**2;
          lam_1_2 = np.sqrt( ((m12_2-(m1_2+m2_2))**2-4*m1_2*m2_2)/(4*m12_2) )
          return lam_1_2

      def three_body_decay_h1AA(self, m23, m0, m1, mA, m_delta, y01, y23):
          p1 = self.lamd_1_2(m23, mA, mA); p2 = self.lamd_1_2(m0, m23, m1);
          ##Be careful, we have 4 contraction ways in total.
          return 1.0/(32*np.pi**3*m0**2)*( (4*y01*y23/m_delta**2)**2 )*p1*p2
          

      def h2_h1AA_decay_width(self):
          m22, mA2, mA, m2, m1 = self.m22, self.mA2, self.mA, self.m2, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m2.shape); err = np.zeros(m2.shape);
          mc = 1.275*np.ones(m2.shape);
          idx1 = np.where((m2 > m1 +2*mA) & (mA > m1/2))
          ###np.vectorize(self.three_body_decay_h1AA)
          ###Gamma[idx1], err[idx1] = integrate.quad(self.three_body_decay_h1AA, 2*mA[idx1], (m2-m1)[idx1], args=(m2[idx1], m1[idx1], mA[idx1], m1[idx1], g_211[idx1], g_1AA[idx1]))
          for i in range(0, idx1[0].shape[0]):
              Gamma[idx1[0][i]], err[idx1[0][i]] = integrate.quad(self.three_body_decay_h1AA, 2*mA[idx1[0][i]], m2[idx1[0][i]]-m1[idx1[0][i]], args=(m2[idx1[0][i]], m1[idx1[0][i]], mA[idx1[0][i]], m1[idx1[0][i]], g_211[idx1[0][i]], g_1AA[idx1[0][i]]))
          self.gamma_h1AA=Gamma
          return Gamma

      def yuk(self, m):
          y=math.sqrt(2)*m/246.0
          return y


      def three_body_decay_h1ff(self, m23, m0, m1, mA, m_delta, y01):
          ##Consider only charm quark
          p1 = self.lamd_1_2(m23, 0, 0); p2 = self.lamd_1_2(m0, m23, m1);
          mc = 1.27
          y2_total = self.yuk(mc)**2
          Nc=3
          ##Be careful, we have 2 contraction ways in total 2*y01.
          return Nc*1.0/(32*math.pi**3*m0**2)*( 2*m23**2*(y2_total)*(2*y01/m0**2)**2 )*p1*p2



      def three_body_decay_h1tt(self, m23, m0, m1, mA, m_delta, y01):
          ##Consider top quark
          mt = 174.0
          p1 = self.lamd_1_2(m23, mt, mt); p2 = self.lamd_1_2(m0, m23, m1);
          y2_total = self.yuk(mt)**2
          Nc=3
          ##Be careful, we have 2 contraction ways in total 2*y01.
          return Nc*1.0/(32*math.pi**3*m0**2)*( 2*m23**2*(y2_total)*(2*y01/m0**2)**2 )*p1*p2



      def h2_h1ff_decay_width(self):
          m22, mA2, mA, m2, m1 = self.m22, self.mA2, self.mA, self.m2, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m2.shape); err = np.zeros(m2.shape);
          idx1 = np.where((m2 > m1 +2*1.27))
          ###print('idx2cccccccc{}'.format(idx1))
          three_body_decay_h1ff = np.vectorize(self.three_body_decay_h1ff)
          mc = 1.27*np.ones(m2.shape)
          ###Gamma[idx1], err[idx1] = integrate.quad(three_body_decay_h1ff, 2*mc[idx1], m2[idx1]-m1[idx1], args=(m2[idx1], m1[idx1], mc[idx1], m1[idx1], g_211[idx1]))        
          for i in range(0, idx1[0].shape[0]):
              Gamma[idx1[0][i]], err[idx1[0][i]] = integrate.quad(self.three_body_decay_h1ff, 2*mc[idx1[0][i]], m2[idx1[0][i]]-m1[idx1[0][i]], args=(m2[idx1[0][i]], m1[idx1[0][i]], mA[idx1[0][i]], m1[idx1[0][i]], g_211[idx1[0][i]]))
          self.gamma_h1ff=Gamma
          return Gamma



      def h2_h1tt_decay_width(self):
          m22, mA2, mA, m2, m1 = self.m22, self.mA2, self.mA, self.m2, self.m1
          g_111, g_211, g_1AA, g_2AA = self.cxSM_coupling()
          Gamma = np.zeros(m2.shape); err = np.zeros(m2.shape);
          idx1 = np.where((m2 > m1+2*174.0))
          three_body_decay_h1tt = np.vectorize(self.three_body_decay_h1tt)
          mt = np.ones(m2.shape)*174.0
          ###Gamma[idx1], err[idx1] = integrate.quad(three_body_decay_h1tt, 2*mt[idx1], m2[idx1]-m1[idx1], args=(m2[idx1], m1[idx1], mt[idx1], m1[idx1], g_211[idx1]))             
          for i in range(0, idx1[0].shape[0]):
              Gamma[idx1[0][i]], err[idx1[0][i]] = integrate.quad(self.three_body_decay_h1tt, 2*mt[idx1[0][i]], m2[idx1[0][i]]-m1[idx1[0][i]], args=(m2[idx1[0][i]], m1[idx1[0][i]], mA[idx1[0][i]], m1[idx1[0][i]], g_211[idx1[0][i]]))
          self.gamma_h1tt=Gamma
          return Gamma



      def h2_2body_decay_dw(self):
          h2_h1h1_dw = self.h2_h1h1_decay_width(); h2_AA_dw = self.h2_AA_decay_width();
          total_h2_decay_width = self.sin_theta**2*self.higgs_like_decay_width + h2_h1h1_dw + h2_AA_dw
          return total_h2_decay_width



      def BR_h2_h1h1(self):
          h2_h1h1_dw = self.h2_h1h1_decay_width(); h2_AA_dw = self.h2_AA_decay_width()
          ###h2_h1AA_dw = self.gamma_h1AA; h2_h1ff_dw = self.gamma_h1ff; h2_h1tt_dw = self.gamma_h1tt
          total_h2_decay_width = self.sin_theta**2*self.higgs_like_decay_width + h2_h1h1_dw + h2_AA_dw
          '''
          for i in range(0, len(total_h2_decay_width)):
              if total_h2_decay_width[i]==0:
                 print(total_h2_decay_width)
          '''
          ##print('h2 to higgs width={}'.format(h2_h1h1_dw))
          ##print('h2 to AA width={}'.format(h2_AA_dw))
          ##print('total h2 decay width={}'.format(total_h2_decay_width))
          br_h2_h1h1 = h2_h1h1_dw/total_h2_decay_width
          return br_h2_h1h1


      def BR_h2_h1AA(self, h2_h1AA_dw, h2_h1ff_dw, h2_h1tt_dw):
          h2_h1h1_dw = self.h2_h1h1_decay_width(); h2_AA_dw = self.h2_AA_decay_width()
          total_h2_decay_width = self.sin_theta**2*self.higgs_like_decay_width + h2_h1h1_dw + h2_AA_dw + h2_h1AA_dw + h2_h1ff_dw + h2_h1tt_dw
          br_h2_h1AA = h2_h1AA_dw/total_h2_decay_width
          return br_h2_h1AA

      def cro_sec_h2_h1AA(self):
          total_cro_sec = self.h2_produce_cro_sec
          br_h2_h1AA = self.BR_h2_h1AA()
          return br_h2_h1AA*total_cro_sec

      def cro_sec_h2_h1h1(self):
          total_cro_sec = self.h2_produce_cro_sec     
          br_h2_h1h1 = self.BR_h2_h1h1()   
          return br_h2_h1h1*total_cro_sec    

      def BR_h1_AA(self):
          h1_AA_dw = self.h1_AA_decay_width()
          total_h1_decay_width = self.cos_theta**2*self.higgs_SM_decay_width + h1_AA_dw
          br_h1_AA = h1_AA_dw/total_h1_decay_width
          return br_h1_AA

      ###This gives the cross section of p p > h2 > h1 h1, h1 > SM SM, h1 > A A
      def cro_sec_final(self):
          br_h2_h1h1 = self.BR_h2_h1h1(); br_h1_AA = self.BR_h1_AA();
          cro_sec = self.h2_produce_cro_sec*br_h2_h1h1*br_h1_AA*0.58*2
          return cro_sec


      def SM_like_h1_total_decay_width(self):
          y = self.cos_theta**2*self.higgs_SM_decay_width + self.h1_AA_decay_width();
          return y


      def sig_stgth(self):
          h1_dw = self.SM_like_h1_total_decay_width()
          y = self.cos_theta**4*(self.higgs_SM_decay_width)/h1_dw
          return y





