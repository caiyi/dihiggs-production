# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Linux x86 (64-bit) (July 15, 2020)
# Date: Fri 21 May 2021 22:47:33


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



