# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Linux x86 (64-bit) (July 15, 2020)
# Date: Fri 21 May 2021 22:47:33


from object_library import all_decays, Decay
import particles as P


Decay_b = Decay(name = 'Decay_b',
                particle = P.b,
                partial_widths = {(P.W__minus__,P.t):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MB)**3)'})

Decay_H1 = Decay(name = 'Decay_H1',
                 particle = P.H1,
                 partial_widths = {(P.P__tilde__Ah,P.P__tilde__Ah):'(((COS**2*delta2**2*vev**2)/4. + (COS*d2*delta2*SIN*vev*x0)/2. + (d2**2*SIN**2*x0**2)/4.)*cmath.sqrt(-4*MAh**2*MH**2 + MH**4))/(32.*cmath.pi*abs(MH)**3)',
                                   (P.b,P.b__tilde__):'((-12*COS**2*MB**2*yb**2 + 3*COS**2*MH**2*yb**2)*cmath.sqrt(-4*MB**2*MH**2 + MH**4))/(16.*cmath.pi*abs(MH)**3)',
                                   (P.h2,P.h2):'(((COS**6*delta2**2*vev**2)/4. - COS**4*delta2**2*SIN**2*vev**2 + (3*COS**4*delta2*lam*SIN**2*vev**2)/2. + COS**2*delta2**2*SIN**4*vev**2 - 3*COS**2*delta2*lam*SIN**4*vev**2 + (9*COS**2*lam**2*SIN**4*vev**2)/4. + (3*COS**5*d2*delta2*SIN*vev*x0)/2. - COS**5*delta2**2*SIN*vev*x0 - 3*COS**3*d2*delta2*SIN**3*vev*x0 + (5*COS**3*delta2**2*SIN**3*vev*x0)/2. + (9*COS**3*d2*lam*SIN**3*vev*x0)/2. - 3*COS**3*delta2*lam*SIN**3*vev*x0 - COS*delta2**2*SIN**5*vev*x0 + (3*COS*delta2*lam*SIN**5*vev*x0)/2. + (9*COS**4*d2**2*SIN**2*x0**2)/4. - 3*COS**4*d2*delta2*SIN**2*x0**2 + COS**4*delta2**2*SIN**2*x0**2 + (3*COS**2*d2*delta2*SIN**4*x0**2)/2. - COS**2*delta2**2*SIN**4*x0**2 + (delta2**2*SIN**6*x0**2)/4.)*cmath.sqrt(MH**4 - 4*MH**2*Mh2**2))/(32.*cmath.pi*abs(MH)**3)',
                                   (P.ta__minus__,P.ta__plus__):'((COS**2*MH**2*ytau**2 - 4*COS**2*MTA**2*ytau**2)*cmath.sqrt(MH**4 - 4*MH**2*MTA**2))/(16.*cmath.pi*abs(MH)**3)',
                                   (P.t,P.t__tilde__):'((3*COS**2*MH**2*yt**2 - 12*COS**2*MT**2*yt**2)*cmath.sqrt(MH**4 - 4*MH**2*MT**2))/(16.*cmath.pi*abs(MH)**3)',
                                   (P.W__minus__,P.W__plus__):'(((3*COS**2*ee**4*vev**2)/(4.*sw**4) + (COS**2*ee**4*MH**4*vev**2)/(16.*MW**4*sw**4) - (COS**2*ee**4*MH**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(MH**4 - 4*MH**2*MW**2))/(16.*cmath.pi*abs(MH)**3)',
                                   (P.Z,P.Z):'(((9*COS**2*ee**4*vev**2)/2. + (3*COS**2*ee**4*MH**4*vev**2)/(8.*MZ**4) - (3*COS**2*ee**4*MH**2*vev**2)/(2.*MZ**2) + (3*COS**2*cw**4*ee**4*vev**2)/(4.*sw**4) + (COS**2*cw**4*ee**4*MH**4*vev**2)/(16.*MZ**4*sw**4) - (COS**2*cw**4*ee**4*MH**2*vev**2)/(4.*MZ**2*sw**4) + (3*COS**2*cw**2*ee**4*vev**2)/sw**2 + (COS**2*cw**2*ee**4*MH**4*vev**2)/(4.*MZ**4*sw**2) - (COS**2*cw**2*ee**4*MH**2*vev**2)/(MZ**2*sw**2) + (3*COS**2*ee**4*sw**2*vev**2)/cw**2 + (COS**2*ee**4*MH**4*sw**2*vev**2)/(4.*cw**2*MZ**4) - (COS**2*ee**4*MH**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*COS**2*ee**4*sw**4*vev**2)/(4.*cw**4) + (COS**2*ee**4*MH**4*sw**4*vev**2)/(16.*cw**4*MZ**4) - (COS**2*ee**4*MH**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_h2 = Decay(name = 'Decay_h2',
                 particle = P.h2,
                 partial_widths = {(P.P__tilde__Ah,P.P__tilde__Ah):'(((delta2**2*SIN**2*vev**2)/4. - (COS*d2*delta2*SIN*vev*x0)/2. + (COS**2*d2**2*x0**2)/4.)*cmath.sqrt(-4*MAh**2*Mh2**2 + Mh2**4))/(32.*cmath.pi*abs(Mh2)**3)',
                                   (P.b,P.b__tilde__):'((-12*MB**2*SIN**2*yb**2 + 3*Mh2**2*SIN**2*yb**2)*cmath.sqrt(-4*MB**2*Mh2**2 + Mh2**4))/(16.*cmath.pi*abs(Mh2)**3)',
                                   (P.H1,P.H1):'((COS**4*delta2**2*SIN**2*vev**2 - 3*COS**4*delta2*lam*SIN**2*vev**2 + (9*COS**4*lam**2*SIN**2*vev**2)/4. - COS**2*delta2**2*SIN**4*vev**2 + (3*COS**2*delta2*lam*SIN**4*vev**2)/2. + (delta2**2*SIN**6*vev**2)/4. + COS**5*delta2**2*SIN*vev*x0 - (3*COS**5*delta2*lam*SIN*vev*x0)/2. + 3*COS**3*d2*delta2*SIN**3*vev*x0 - (5*COS**3*delta2**2*SIN**3*vev*x0)/2. - (9*COS**3*d2*lam*SIN**3*vev*x0)/2. + 3*COS**3*delta2*lam*SIN**3*vev*x0 - (3*COS*d2*delta2*SIN**5*vev*x0)/2. + COS*delta2**2*SIN**5*vev*x0 + (COS**6*delta2**2*x0**2)/4. + (3*COS**4*d2*delta2*SIN**2*x0**2)/2. - COS**4*delta2**2*SIN**2*x0**2 + (9*COS**2*d2**2*SIN**4*x0**2)/4. - 3*COS**2*d2*delta2*SIN**4*x0**2 + COS**2*delta2**2*SIN**4*x0**2)*cmath.sqrt(-4*MH**2*Mh2**2 + Mh2**4))/(32.*cmath.pi*abs(Mh2)**3)',
                                   (P.ta__minus__,P.ta__plus__):'((Mh2**2*SIN**2*ytau**2 - 4*MTA**2*SIN**2*ytau**2)*cmath.sqrt(Mh2**4 - 4*Mh2**2*MTA**2))/(16.*cmath.pi*abs(Mh2)**3)',
                                   (P.t,P.t__tilde__):'((3*Mh2**2*SIN**2*yt**2 - 12*MT**2*SIN**2*yt**2)*cmath.sqrt(Mh2**4 - 4*Mh2**2*MT**2))/(16.*cmath.pi*abs(Mh2)**3)',
                                   (P.W__minus__,P.W__plus__):'(((3*ee**4*SIN**2*vev**2)/(4.*sw**4) + (ee**4*Mh2**4*SIN**2*vev**2)/(16.*MW**4*sw**4) - (ee**4*Mh2**2*SIN**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(Mh2**4 - 4*Mh2**2*MW**2))/(16.*cmath.pi*abs(Mh2)**3)',
                                   (P.Z,P.Z):'(((9*ee**4*SIN**2*vev**2)/2. + (3*ee**4*Mh2**4*SIN**2*vev**2)/(8.*MZ**4) - (3*ee**4*Mh2**2*SIN**2*vev**2)/(2.*MZ**2) + (3*cw**4*ee**4*SIN**2*vev**2)/(4.*sw**4) + (cw**4*ee**4*Mh2**4*SIN**2*vev**2)/(16.*MZ**4*sw**4) - (cw**4*ee**4*Mh2**2*SIN**2*vev**2)/(4.*MZ**2*sw**4) + (3*cw**2*ee**4*SIN**2*vev**2)/sw**2 + (cw**2*ee**4*Mh2**4*SIN**2*vev**2)/(4.*MZ**4*sw**2) - (cw**2*ee**4*Mh2**2*SIN**2*vev**2)/(MZ**2*sw**2) + (3*ee**4*SIN**2*sw**2*vev**2)/cw**2 + (ee**4*Mh2**4*SIN**2*sw**2*vev**2)/(4.*cw**2*MZ**4) - (ee**4*Mh2**2*SIN**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*ee**4*SIN**2*sw**4*vev**2)/(4.*cw**4) + (ee**4*Mh2**4*SIN**2*sw**4*vev**2)/(16.*cw**4*MZ**4) - (ee**4*Mh2**2*SIN**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(Mh2**4 - 4*Mh2**2*MZ**2))/(32.*cmath.pi*abs(Mh2)**3)'})

Decay_t = Decay(name = 'Decay_t',
                particle = P.t,
                partial_widths = {(P.W__plus__,P.b):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MT)**3)'})

Decay_ta__minus__ = Decay(name = 'Decay_ta__minus__',
                          particle = P.ta__minus__,
                          partial_widths = {(P.W__minus__,P.vt):'((MTA**2 - MW**2)*((ee**2*MTA**2)/(2.*sw**2) + (ee**2*MTA**4)/(2.*MW**2*sw**2) - (ee**2*MW**2)/sw**2))/(32.*cmath.pi*abs(MTA)**3)'})

Decay_W__plus__ = Decay(name = 'Decay_W__plus__',
                        particle = P.W__plus__,
                        partial_widths = {(P.c,P.s__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.t,P.b__tilde__):'(((-3*ee**2*MB**2)/(2.*sw**2) - (3*ee**2*MT**2)/(2.*sw**2) - (3*ee**2*MB**4)/(2.*MW**2*sw**2) + (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) - (3*ee**2*MT**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.u,P.d__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.ve,P.e__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vm,P.mu__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vt,P.ta__plus__):'((-MTA**2 + MW**2)*(-(ee**2*MTA**2)/(2.*sw**2) - (ee**2*MTA**4)/(2.*MW**2*sw**2) + (ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)'})

Decay_Z = Decay(name = 'Decay_Z',
                particle = P.Z,
                partial_widths = {(P.b,P.b__tilde__):'((-7*ee**2*MB**2 + ee**2*MZ**2 - (3*cw**2*ee**2*MB**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) - (17*ee**2*MB**2*sw**2)/(6.*cw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MB**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.c,P.c__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.d,P.d__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.e__minus__,P.e__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.mu__minus__,P.mu__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.s,P.s__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((-5*ee**2*MTA**2 - ee**2*MZ**2 - (cw**2*ee**2*MTA**2)/(2.*sw**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MTA**2*sw**2)/(2.*cw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2))*cmath.sqrt(-4*MTA**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.t,P.t__tilde__):'((-11*ee**2*MT**2 - ee**2*MZ**2 - (3*cw**2*ee**2*MT**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MT**2*sw**2)/(6.*cw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MT**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.u,P.u__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ve,P.ve__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vm,P.vm__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vt,P.vt__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.W__minus__,P.W__plus__):'(((-12*cw**2*ee**2*MW**2)/sw**2 - (17*cw**2*ee**2*MZ**2)/sw**2 + (4*cw**2*ee**2*MZ**4)/(MW**2*sw**2) + (cw**2*ee**2*MZ**6)/(4.*MW**4*sw**2))*cmath.sqrt(-4*MW**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)'})

